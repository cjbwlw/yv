/*
 Navicat Premium Data Transfer

 Source Server         : 本机
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : localhost:3306
 Source Schema         : yv_system

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 22/11/2023 12:36:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` bigint(20) NOT NULL,
  `p_id` bigint(20) NOT NULL COMMENT '父id',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `title` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单标题',
  `icon` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'icon图标',
  `url` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'url地址',
  `path` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '前端路由',
  `component` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组件路径',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0禁用，1启用',
  `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0未删除，1删除',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建用户',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新用户',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 0, 'system', '系统管理', 'setting', '/', '/system', 'index.vue', 1, 0, '2023-10-11 09:27:34', 1, '2023-11-14 10:18:18', NULL);
INSERT INTO `menu` VALUES (100, 1, 'menu', '菜单管理', 'menu', '/system/menu', 'menu', 'system/menu/index.vue', 1, 0, '2023-10-11 09:28:02', 1, '2023-10-22 17:53:11', 1);
INSERT INTO `menu` VALUES (102, 1, 'user', '用户管理', 'user', '/system/user', 'user', 'system/user/index.vue', 1, 0, '2023-10-15 15:23:40', 1, '2023-10-22 18:00:46', 1);
INSERT INTO `menu` VALUES (1716086868233805825, 1, 'role', '角色管理', 'Avatar', '/system/role', 'role', 'system/role/index.vue', 1, 0, '2023-10-22 21:39:27', NULL, '2023-10-27 19:07:51', NULL);
INSERT INTO `menu` VALUES (1726061724303986689, 0, 'chat', '聊天管理', 'ChatDotSquare', '/chat', '/chat', 'index.vue', 1, 0, '2023-11-19 10:15:58', NULL, '2023-11-19 10:15:58', NULL);
INSERT INTO `menu` VALUES (1726062691162361857, 1726061724303986689, 'friendsCircle', '朋友圈管理', 'Compass', '/chat/friendsCircle', 'friendsCircle', 'chat/friendsCircle/index.vue', 1, 0, '2023-11-19 10:19:49', NULL, '2023-11-19 10:19:54', NULL);
INSERT INTO `menu` VALUES (1726116180609568770, 1726061724303986689, 'comment', '评论管理', 'ChatSquare', '/chat/comment', 'comment', 'chat/comment/index.vue', 1, 0, '2023-11-19 13:52:22', NULL, '2023-11-19 13:52:46', NULL);
INSERT INTO `menu` VALUES (1726130823524618242, 1726061724303986689, 'applyFriend', '好友申请管理', 'Avatar', '/chat/applyFriend', 'applyFriend', 'chat/applyFriend/index.vue', 1, 0, '2023-11-19 14:50:33', NULL, '2023-11-19 14:50:33', NULL);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` bigint(20) NOT NULL,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `authority` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'select' COMMENT '权限:all,insert,update,delete,select',
  `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(20) NULL DEFAULT NULL COMMENT '创建用户',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `update_user` bigint(20) NULL DEFAULT NULL COMMENT '更新用户',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, '超级管理员', 'root', 0, '2023-10-21 10:45:28', 1, '2023-10-23 17:52:42', NULL);
INSERT INTO `role` VALUES (2, 'vip用户', 'test', 0, '2023-10-24 09:52:11', NULL, '2023-10-24 09:52:11', NULL);
INSERT INTO `role` VALUES (3, '普通用户', '123', 0, '2023-10-23 09:42:31', 1, '2023-10-24 13:28:55', NULL);

-- ----------------------------
-- Table structure for u_f
-- ----------------------------
DROP TABLE IF EXISTS `u_f`;
CREATE TABLE `u_f`  (
  `u_id` bigint(20) NOT NULL COMMENT '当前用户',
  `f_id` bigint(20) NOT NULL COMMENT '好友'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of u_f
-- ----------------------------
INSERT INTO `u_f` VALUES (2, 1);
INSERT INTO `u_f` VALUES (1, 2);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(20) NOT NULL,
  `rid` bigint(20) NOT NULL DEFAULT 3 COMMENT '角色id',
  `name` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像url',
  `nickname` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '昵称',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '123456' COMMENT '登录密码',
  `ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录IP',
  `login_time` datetime NULL DEFAULT NULL COMMENT '登录时间',
  `gender` tinyint(1) NULL DEFAULT 1 COMMENT '性别，1男，0女',
  `id_no` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号码',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `email` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态，1可用，0禁用',
  `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0未删除，1删除',
  `register_time` datetime NULL DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 1, 'YV', 'https://8v249k2561.goho.co/api/system/file/preview?path=img%5C2023%5C11%5C21%5C170056973841459477.jpeg', '小晨', '84E091C391E0B4C439D687767808DEBF', '192.168.1.104', '2023-11-22 12:27:01', 1, '431028200211191314', '13356881435', 'cjbwlw@163.com', 1, 0, '2022-07-13 10:45:46');
INSERT INTO `user` VALUES (2, 3, 'xw', 'https://8v249k2561.goho.co/api/system/file/preview?path=img%5C2023%5C11%5C22%5C170062596642348317.png', 'xw', '84E091C391E0B4C439D687767808DEBF', '192.168.1.104', '2023-11-22 12:23:23', 1, NULL, NULL, NULL, 1, 0, '2023-11-19 20:22:32');

SET FOREIGN_KEY_CHECKS = 1;
