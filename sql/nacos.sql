/*
 Navicat Premium Data Transfer

 Source Server         : 本机
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : localhost:3306
 Source Schema         : nacos

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 22/11/2023 12:36:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for config_info
-- ----------------------------
DROP TABLE IF EXISTS `config_info`;
CREATE TABLE `config_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  `c_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `c_use` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `effect` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `c_schema` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `encrypted_data_key` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '秘钥',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfo_datagrouptenant`(`data_id`, `group_id`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info
-- ----------------------------
INSERT INTO `config_info` VALUES (7, 'dataSource.yml', 'dev', 'spring:\n  datasource:\n    druid:\n      driver-class-name:  com.mysql.cj.jdbc.Driver\n      url: jdbc:mysql://127.0.0.1:3306/yv_system?characterEncoding=utf8&serverTimezone=UTC  # 连接数据库\n      username: root    # 数据库账户\n      password: 123456    # 数据库密码\n      stat-view-servlet:\n        login-username: admin # 登录账号\n        login-password: 123456 # 登录密码\n        reset-enable:  false # 重置监控页面数据\n        url-pattern: /druid/* # 登录页面后缀\n        enabled: true  # 开启监控\n        allow: # 添加IP白名单,不写就是所有都允许\n          - 127.0.0.1\n      #状态监控\n      filter:\n        stat:\n          # 慢SQL记录\n          log-slow-sql: true\n          slow-sql-millis: 1000\n          merge-sql: true\n          enabled: true\n        wall:\n          config:\n            multi-statement-allow: true\n      # 监控配置中的 web监控\n      web-stat-filter:\n        url-pattern: /*\n        exclusions: \"*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*\"\n        enabled: true\n      initial-size: 10\n      max-active: 100\n      max-wait: 6000\n      min-idle: 1\n      #配置一个连接在池中最大，小生存时间，单位是毫秒\n      max-evictable-idle-time-millis: 1800000\n      min-evictable-idle-time-millis: 500000\n  mvc:\n    pathmatch:\n      matching-strategy: ant_path_matcher # 路径匹配策略:解决开发接口文档启动失败', '57ec1e64f6d6a2e2b61a8134e97bc966', '2023-10-21 14:12:18', '2023-11-20 04:28:20', '', '127.0.0.1', '', '0bae2241-eb64-46f0-8acf-64b0f1e34007', '', '', '', 'yaml', '', '');
INSERT INTO `config_info` VALUES (8, 'mybatis.yml', 'dev', 'mybatis-plus:\r\n  global-config:\r\n    banner: false\r\n    db-config:\r\n      logic-not-delete-value: 0 #逻辑删除\r\n      logic-delete-value: 1\r\n  mapper-locations: classpath*:/mapper/**/*.xml', '39f5a0d192d8f530daa4639abb399d00', '2023-10-21 14:13:01', '2023-10-21 14:13:01', NULL, '127.0.0.1', '', '0bae2241-eb64-46f0-8acf-64b0f1e34007', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (9, 'other.yml', 'dev', 'logging:\r\n  level:\r\n    pers.cxl: debug', '44741e4d26ee65f6070aab86d9f18db8', '2023-10-21 14:13:29', '2023-10-21 14:13:29', NULL, '127.0.0.1', '', '0bae2241-eb64-46f0-8acf-64b0f1e34007', NULL, NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (11, 'application.yml', 'dev', 'spring:\n  datasource:\n    druid:\n      driver-class-name:  com.mysql.cj.jdbc.Driver\n      url: jdbc:mysql://127.0.0.1:3306/yv_app?characterEncoding=utf8&serverTimezone=UTC  # 连接数据库\n      username: root    # 数据库账户\n      password: 123456    # 数据库密码\n      stat-view-servlet:\n        login-username: admin # 登录账号\n        login-password: 123456 # 登录密码\n        reset-enable:  false # 重置监控页面数据\n        url-pattern: /druid/* # 登录页面后缀\n        enabled: true  # 开启监控\n        allow: # 添加IP白名单,不写就是所有都允许\n          - 127.0.0.1\n      #状态监控\n      filter:\n        stat:\n          # 慢SQL记录\n          log-slow-sql: true\n          slow-sql-millis: 1000\n          merge-sql: true\n          enabled: true\n        wall:\n          config:\n            multi-statement-allow: true\n      # 监控配置中的 web监控\n      web-stat-filter:\n        url-pattern: /*\n        exclusions: \"*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*\"\n        enabled: true\n      initial-size: 10\n      max-active: 100\n      max-wait: 6000\n      min-idle: 1\n      #配置一个连接在池中最大，小生存时间，单位是毫秒\n      max-evictable-idle-time-millis: 1800000\n      min-evictable-idle-time-millis: 500000\n  mvc:\n    pathmatch:\n      matching-strategy: ant_path_matcher # 路径匹配策略:解决开发接口文档启动失败', 'b5c8ba4a0288dc60e287a39089aa25a5', '2023-11-14 04:18:19', '2023-11-20 04:33:35', '', '127.0.0.1', '', '603c1a9a-e1f8-4b40-aaf9-27da269a2738', '', '', '', 'yaml', '', '');

-- ----------------------------
-- Table structure for config_info_aggr
-- ----------------------------
DROP TABLE IF EXISTS `config_info_aggr`;
CREATE TABLE `config_info_aggr`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `datum_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'datum_id',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '内容',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfoaggr_datagrouptenantdatum`(`data_id`, `group_id`, `tenant_id`, `datum_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '增加租户字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info_aggr
-- ----------------------------

-- ----------------------------
-- Table structure for config_info_beta
-- ----------------------------
DROP TABLE IF EXISTS `config_info_beta`;
CREATE TABLE `config_info_beta`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `beta_ips` varchar(1024) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'betaIps',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  `encrypted_data_key` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '秘钥',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfobeta_datagrouptenant`(`data_id`, `group_id`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info_beta' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info_beta
-- ----------------------------

-- ----------------------------
-- Table structure for config_info_tag
-- ----------------------------
DROP TABLE IF EXISTS `config_info_tag`;
CREATE TABLE `config_info_tag`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tag_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'tag_id',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfotag_datagrouptenanttag`(`data_id`, `group_id`, `tenant_id`, `tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info_tag' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info_tag
-- ----------------------------

-- ----------------------------
-- Table structure for config_tags_relation
-- ----------------------------
DROP TABLE IF EXISTS `config_tags_relation`;
CREATE TABLE `config_tags_relation`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `tag_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'tag_name',
  `tag_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'tag_type',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `nid` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nid`) USING BTREE,
  UNIQUE INDEX `uk_configtagrelation_configidtag`(`id`, `tag_name`, `tag_type`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_tag_relation' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_tags_relation
-- ----------------------------

-- ----------------------------
-- Table structure for group_capacity
-- ----------------------------
DROP TABLE IF EXISTS `group_capacity`;
CREATE TABLE `group_capacity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Group ID，空字符表示整个集群',
  `quota` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数，，0表示使用默认值',
  `max_aggr_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_group_id`(`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '集群、各Group容量信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of group_capacity
-- ----------------------------

-- ----------------------------
-- Table structure for his_config_info
-- ----------------------------
DROP TABLE IF EXISTS `his_config_info`;
CREATE TABLE `his_config_info`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `src_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `op_type` char(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  `encrypted_data_key` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '秘钥',
  PRIMARY KEY (`nid`) USING BTREE,
  INDEX `idx_gmt_create`(`gmt_create`) USING BTREE,
  INDEX `idx_gmt_modified`(`gmt_modified`) USING BTREE,
  INDEX `idx_did`(`data_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '多租户改造' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of his_config_info
-- ----------------------------
INSERT INTO `his_config_info` VALUES (7, 16, 'dataSource.yml', 'dev', '', 'spring:\r\n  datasource:\r\n    druid:\r\n      driver-class-name:  com.mysql.cj.jdbc.Driver\r\n      url: jdbc:mysql://127.0.0.1:3306/yv_system?characterEncoding=utf8&serverTimezone=UTC  # 连接数据库\r\n      username: root    # 数据库账户\r\n      password: 123456    # 数据库密码\r\n      stat-view-servlet:\r\n        login-username: admin # 登录账号\r\n        login-password: 123456 # 登录密码\r\n        reset-enable:  false # 重置监控页面数据\r\n        url-pattern: /druid/* # 登录页面后缀\r\n        enabled: true  # 开启监控\r\n        allow: # 添加IP白名单,不写就是所有都允许\r\n          - 127.0.0.1\r\n      #状态监控\r\n      filter:\r\n        stat:\r\n          # 慢SQL记录\r\n          log-slow-sql: true\r\n          slow-sql-millis: 1000\r\n          merge-sql: true\r\n          enabled: true\r\n        wall:\r\n          config:\r\n            multi-statement-allow: true\r\n      # 监控配置中的 web监控\r\n      web-stat-filter:\r\n        url-pattern: /*\r\n        exclusions: \"*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*\"\r\n        enabled: true\r\n      initial-size: 10\r\n      max-active: 100\r\n      max-wait: 6000\r\n      min-idle: 1\r\n      #配置一个连接在池中最大，小生存时间，单位是毫秒\r\n      max-evictable-idle-time-millis: 1800000\r\n      min-evictable-idle-time-millis: 500000\r\n  mvc:\r\n    pathmatch:\r\n      matching-strategy: ant_path_matcher # 路径匹配策略:解决开发接口文档启动失败\r\n  jackson:\r\n    date-format: yyyy-MM-dd HH:mm:ss\r\n    time-zone: GMT+8\r\n    generator:\r\n      write-numbers-as-strings: true', '3d0392aba394ce3a2356b3eeca9e8ca2', '2023-10-27 15:07:13', '2023-10-27 07:07:13', '', '127.0.0.1', 'U', '0bae2241-eb64-46f0-8acf-64b0f1e34007', '');
INSERT INTO `his_config_info` VALUES (7, 17, 'dataSource.yml', 'dev', '', 'spring:\n  datasource:\n    druid:\n      driver-class-name:  com.mysql.cj.jdbc.Driver\n      url: jdbc:mysql://127.0.0.1:3306/yv_system?characterEncoding=utf8&serverTimezone=UTC  # 连接数据库\n      username: root    # 数据库账户\n      password: 123456    # 数据库密码\n      stat-view-servlet:\n        login-username: admin # 登录账号\n        login-password: 123456 # 登录密码\n        reset-enable:  false # 重置监控页面数据\n        url-pattern: /druid/* # 登录页面后缀\n        enabled: true  # 开启监控\n        allow: # 添加IP白名单,不写就是所有都允许\n          - 127.0.0.1\n      #状态监控\n      filter:\n        stat:\n          # 慢SQL记录\n          log-slow-sql: true\n          slow-sql-millis: 1000\n          merge-sql: true\n          enabled: true\n        wall:\n          config:\n            multi-statement-allow: true\n      # 监控配置中的 web监控\n      web-stat-filter:\n        url-pattern: /*\n        exclusions: \"*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*\"\n        enabled: true\n      initial-size: 10\n      max-active: 100\n      max-wait: 6000\n      min-idle: 1\n      #配置一个连接在池中最大，小生存时间，单位是毫秒\n      max-evictable-idle-time-millis: 1800000\n      min-evictable-idle-time-millis: 500000\n  mvc:\n    pathmatch:\n      matching-strategy: ant_path_matcher # 路径匹配策略:解决开发接口文档启动失败\n  jackson:\n    date-format: yyyy-MM-dd HH:mm:ss\n    time-zone: GMT+8', '06658eed7ea2bd13c638caee44ee1cce', '2023-10-27 15:41:02', '2023-10-27 07:41:03', 'nacos', '127.0.0.1', 'U', '0bae2241-eb64-46f0-8acf-64b0f1e34007', '');
INSERT INTO `his_config_info` VALUES (0, 18, 'application', 'dev', '', 'spring:\r\n  datasource:\r\n    druid:\r\n      driver-class-name:  com.mysql.cj.jdbc.Driver\r\n      url: jdbc:mysql://127.0.0.1:3306/yv_app?characterEncoding=utf8&serverTimezone=UTC  # 连接数据库\r\n      username: root    # 数据库账户\r\n      password: 123456    # 数据库密码\r\n      stat-view-servlet:\r\n        login-username: admin # 登录账号\r\n        login-password: 123456 # 登录密码\r\n        reset-enable:  false # 重置监控页面数据\r\n        url-pattern: /druid/* # 登录页面后缀\r\n        enabled: true  # 开启监控\r\n        allow: # 添加IP白名单,不写就是所有都允许\r\n          - 127.0.0.1\r\n      #状态监控\r\n      filter:\r\n        stat:\r\n          # 慢SQL记录\r\n          log-slow-sql: true\r\n          slow-sql-millis: 1000\r\n          merge-sql: true\r\n          enabled: true\r\n        wall:\r\n          config:\r\n            multi-statement-allow: true\r\n      # 监控配置中的 web监控\r\n      web-stat-filter:\r\n        url-pattern: /*\r\n        exclusions: \"*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*\"\r\n        enabled: true\r\n      initial-size: 10\r\n      max-active: 100\r\n      max-wait: 6000\r\n      min-idle: 1\r\n      #配置一个连接在池中最大，小生存时间，单位是毫秒\r\n      max-evictable-idle-time-millis: 1800000\r\n      min-evictable-idle-time-millis: 500000\r\n  mvc:\r\n    pathmatch:\r\n      matching-strategy: ant_path_matcher # 路径匹配策略:解决开发接口文档启动失败', 'de5a506de14b4fa8aa5a92138047a1bd', '2023-11-14 12:17:43', '2023-11-14 04:17:44', NULL, '192.168.181.1', 'I', '603c1a9a-e1f8-4b40-aaf9-27da269a2738', '');
INSERT INTO `his_config_info` VALUES (10, 19, 'application', 'dev', '', 'spring:\r\n  datasource:\r\n    druid:\r\n      driver-class-name:  com.mysql.cj.jdbc.Driver\r\n      url: jdbc:mysql://127.0.0.1:3306/yv_app?characterEncoding=utf8&serverTimezone=UTC  # 连接数据库\r\n      username: root    # 数据库账户\r\n      password: 123456    # 数据库密码\r\n      stat-view-servlet:\r\n        login-username: admin # 登录账号\r\n        login-password: 123456 # 登录密码\r\n        reset-enable:  false # 重置监控页面数据\r\n        url-pattern: /druid/* # 登录页面后缀\r\n        enabled: true  # 开启监控\r\n        allow: # 添加IP白名单,不写就是所有都允许\r\n          - 127.0.0.1\r\n      #状态监控\r\n      filter:\r\n        stat:\r\n          # 慢SQL记录\r\n          log-slow-sql: true\r\n          slow-sql-millis: 1000\r\n          merge-sql: true\r\n          enabled: true\r\n        wall:\r\n          config:\r\n            multi-statement-allow: true\r\n      # 监控配置中的 web监控\r\n      web-stat-filter:\r\n        url-pattern: /*\r\n        exclusions: \"*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*\"\r\n        enabled: true\r\n      initial-size: 10\r\n      max-active: 100\r\n      max-wait: 6000\r\n      min-idle: 1\r\n      #配置一个连接在池中最大，小生存时间，单位是毫秒\r\n      max-evictable-idle-time-millis: 1800000\r\n      min-evictable-idle-time-millis: 500000\r\n  mvc:\r\n    pathmatch:\r\n      matching-strategy: ant_path_matcher # 路径匹配策略:解决开发接口文档启动失败', 'de5a506de14b4fa8aa5a92138047a1bd', '2023-11-14 12:18:03', '2023-11-14 04:18:03', NULL, '192.168.181.1', 'D', '603c1a9a-e1f8-4b40-aaf9-27da269a2738', '');
INSERT INTO `his_config_info` VALUES (0, 20, 'application.yml', 'dev', '', 'spring:\r\n  datasource:\r\n    druid:\r\n      driver-class-name:  com.mysql.cj.jdbc.Driver\r\n      url: jdbc:mysql://127.0.0.1:3306/yv_app?characterEncoding=utf8&serverTimezone=UTC  # 连接数据库\r\n      username: root    # 数据库账户\r\n      password: 123456    # 数据库密码\r\n      stat-view-servlet:\r\n        login-username: admin # 登录账号\r\n        login-password: 123456 # 登录密码\r\n        reset-enable:  false # 重置监控页面数据\r\n        url-pattern: /druid/* # 登录页面后缀\r\n        enabled: true  # 开启监控\r\n        allow: # 添加IP白名单,不写就是所有都允许\r\n          - 127.0.0.1\r\n      #状态监控\r\n      filter:\r\n        stat:\r\n          # 慢SQL记录\r\n          log-slow-sql: true\r\n          slow-sql-millis: 1000\r\n          merge-sql: true\r\n          enabled: true\r\n        wall:\r\n          config:\r\n            multi-statement-allow: true\r\n      # 监控配置中的 web监控\r\n      web-stat-filter:\r\n        url-pattern: /*\r\n        exclusions: \"*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*\"\r\n        enabled: true\r\n      initial-size: 10\r\n      max-active: 100\r\n      max-wait: 6000\r\n      min-idle: 1\r\n      #配置一个连接在池中最大，小生存时间，单位是毫秒\r\n      max-evictable-idle-time-millis: 1800000\r\n      min-evictable-idle-time-millis: 500000\r\n  mvc:\r\n    pathmatch:\r\n      matching-strategy: ant_path_matcher # 路径匹配策略:解决开发接口文档启动失败', 'de5a506de14b4fa8aa5a92138047a1bd', '2023-11-14 12:18:18', '2023-11-14 04:18:19', NULL, '192.168.181.1', 'I', '603c1a9a-e1f8-4b40-aaf9-27da269a2738', '');
INSERT INTO `his_config_info` VALUES (11, 21, 'application.yml', 'dev', '', 'spring:\r\n  datasource:\r\n    druid:\r\n      driver-class-name:  com.mysql.cj.jdbc.Driver\r\n      url: jdbc:mysql://127.0.0.1:3306/yv_app?characterEncoding=utf8&serverTimezone=UTC  # 连接数据库\r\n      username: root    # 数据库账户\r\n      password: 123456    # 数据库密码\r\n      stat-view-servlet:\r\n        login-username: admin # 登录账号\r\n        login-password: 123456 # 登录密码\r\n        reset-enable:  false # 重置监控页面数据\r\n        url-pattern: /druid/* # 登录页面后缀\r\n        enabled: true  # 开启监控\r\n        allow: # 添加IP白名单,不写就是所有都允许\r\n          - 127.0.0.1\r\n      #状态监控\r\n      filter:\r\n        stat:\r\n          # 慢SQL记录\r\n          log-slow-sql: true\r\n          slow-sql-millis: 1000\r\n          merge-sql: true\r\n          enabled: true\r\n        wall:\r\n          config:\r\n            multi-statement-allow: true\r\n      # 监控配置中的 web监控\r\n      web-stat-filter:\r\n        url-pattern: /*\r\n        exclusions: \"*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*\"\r\n        enabled: true\r\n      initial-size: 10\r\n      max-active: 100\r\n      max-wait: 6000\r\n      min-idle: 1\r\n      #配置一个连接在池中最大，小生存时间，单位是毫秒\r\n      max-evictable-idle-time-millis: 1800000\r\n      min-evictable-idle-time-millis: 500000\r\n  mvc:\r\n    pathmatch:\r\n      matching-strategy: ant_path_matcher # 路径匹配策略:解决开发接口文档启动失败', 'de5a506de14b4fa8aa5a92138047a1bd', '2023-11-20 12:26:53', '2023-11-20 04:26:54', '', '127.0.0.1', 'U', '603c1a9a-e1f8-4b40-aaf9-27da269a2738', '');
INSERT INTO `his_config_info` VALUES (7, 22, 'dataSource.yml', 'dev', '', 'spring:\n  datasource:\n    druid:\n      driver-class-name:  com.mysql.cj.jdbc.Driver\n      url: jdbc:mysql://127.0.0.1:3306/yv_system?characterEncoding=utf8&serverTimezone=UTC  # 连接数据库\n      username: root    # 数据库账户\n      password: 123456    # 数据库密码\n      stat-view-servlet:\n        login-username: admin # 登录账号\n        login-password: 123456 # 登录密码\n        reset-enable:  false # 重置监控页面数据\n        url-pattern: /druid/* # 登录页面后缀\n        enabled: true  # 开启监控\n        allow: # 添加IP白名单,不写就是所有都允许\n          - 127.0.0.1\n      #状态监控\n      filter:\n        stat:\n          # 慢SQL记录\n          log-slow-sql: true\n          slow-sql-millis: 1000\n          merge-sql: true\n          enabled: true\n        wall:\n          config:\n            multi-statement-allow: true\n      # 监控配置中的 web监控\n      web-stat-filter:\n        url-pattern: /*\n        exclusions: \"*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*\"\n        enabled: true\n      initial-size: 10\n      max-active: 100\n      max-wait: 6000\n      min-idle: 1\n      #配置一个连接在池中最大，小生存时间，单位是毫秒\n      max-evictable-idle-time-millis: 1800000\n      min-evictable-idle-time-millis: 500000\n  mvc:\n    pathmatch:\n      matching-strategy: ant_path_matcher # 路径匹配策略:解决开发接口文档启动失败', '57ec1e64f6d6a2e2b61a8134e97bc966', '2023-11-20 12:27:08', '2023-11-20 04:27:09', '', '127.0.0.1', 'U', '0bae2241-eb64-46f0-8acf-64b0f1e34007', '');
INSERT INTO `his_config_info` VALUES (7, 23, 'dataSource.yml', 'dev', '', 'spring:\n  datasource:\n    druid:\n      driver-class-name:  com.mysql.cj.jdbc.Driver\n      url: jdbc:mysql://192.168.1.102:3306/yv_system?characterEncoding=utf8&serverTimezone=UTC  # 连接数据库\n      username: root    # 数据库账户\n      password: 123456    # 数据库密码\n      stat-view-servlet:\n        login-username: admin # 登录账号\n        login-password: 123456 # 登录密码\n        reset-enable:  false # 重置监控页面数据\n        url-pattern: /druid/* # 登录页面后缀\n        enabled: true  # 开启监控\n        allow: # 添加IP白名单,不写就是所有都允许\n          - 127.0.0.1\n      #状态监控\n      filter:\n        stat:\n          # 慢SQL记录\n          log-slow-sql: true\n          slow-sql-millis: 1000\n          merge-sql: true\n          enabled: true\n        wall:\n          config:\n            multi-statement-allow: true\n      # 监控配置中的 web监控\n      web-stat-filter:\n        url-pattern: /*\n        exclusions: \"*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*\"\n        enabled: true\n      initial-size: 10\n      max-active: 100\n      max-wait: 6000\n      min-idle: 1\n      #配置一个连接在池中最大，小生存时间，单位是毫秒\n      max-evictable-idle-time-millis: 1800000\n      min-evictable-idle-time-millis: 500000\n  mvc:\n    pathmatch:\n      matching-strategy: ant_path_matcher # 路径匹配策略:解决开发接口文档启动失败', '6176678606c748438e2733ffbe4bf93f', '2023-11-20 12:28:20', '2023-11-20 04:28:20', '', '127.0.0.1', 'U', '0bae2241-eb64-46f0-8acf-64b0f1e34007', '');
INSERT INTO `his_config_info` VALUES (11, 24, 'application.yml', 'dev', '', 'spring:\n  datasource:\n    druid:\n      driver-class-name:  com.mysql.cj.jdbc.Driver\n      url: jdbc:mysql://192.168.1.102:3306/yv_app?characterEncoding=utf8&serverTimezone=UTC  # 连接数据库\n      username: root    # 数据库账户\n      password: 123456    # 数据库密码\n      stat-view-servlet:\n        login-username: admin # 登录账号\n        login-password: 123456 # 登录密码\n        reset-enable:  false # 重置监控页面数据\n        url-pattern: /druid/* # 登录页面后缀\n        enabled: true  # 开启监控\n        allow: # 添加IP白名单,不写就是所有都允许\n          - 127.0.0.1\n      #状态监控\n      filter:\n        stat:\n          # 慢SQL记录\n          log-slow-sql: true\n          slow-sql-millis: 1000\n          merge-sql: true\n          enabled: true\n        wall:\n          config:\n            multi-statement-allow: true\n      # 监控配置中的 web监控\n      web-stat-filter:\n        url-pattern: /*\n        exclusions: \"*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*\"\n        enabled: true\n      initial-size: 10\n      max-active: 100\n      max-wait: 6000\n      min-idle: 1\n      #配置一个连接在池中最大，小生存时间，单位是毫秒\n      max-evictable-idle-time-millis: 1800000\n      min-evictable-idle-time-millis: 500000\n  mvc:\n    pathmatch:\n      matching-strategy: ant_path_matcher # 路径匹配策略:解决开发接口文档启动失败', 'f528db8ecab0b9c8be1cc15617981b53', '2023-11-20 12:33:35', '2023-11-20 04:33:35', '', '127.0.0.1', 'U', '603c1a9a-e1f8-4b40-aaf9-27da269a2738', '');

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `role` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `resource` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `action` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `uk_role_permission`(`role`, `resource`, `action`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permissions
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `idx_user_role`(`username`, `role`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('nacos', 'ROLE_ADMIN');

-- ----------------------------
-- Table structure for tenant_capacity
-- ----------------------------
DROP TABLE IF EXISTS `tenant_capacity`;
CREATE TABLE `tenant_capacity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Tenant ID',
  `quota` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数',
  `max_aggr_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '租户容量信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tenant_capacity
-- ----------------------------

-- ----------------------------
-- Table structure for tenant_info
-- ----------------------------
DROP TABLE IF EXISTS `tenant_info`;
CREATE TABLE `tenant_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `kp` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'kp',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tenant_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_name',
  `tenant_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'tenant_desc',
  `create_source` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'create_source',
  `gmt_create` bigint(20) NOT NULL COMMENT '创建时间',
  `gmt_modified` bigint(20) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_info_kptenantid`(`kp`, `tenant_id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'tenant_info' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tenant_info
-- ----------------------------
INSERT INTO `tenant_info` VALUES (3, '1', '0bae2241-eb64-46f0-8acf-64b0f1e34007', 'system', '系统微服务', 'nacos', 1697897310307, 1697897310307);
INSERT INTO `tenant_info` VALUES (4, '1', '603c1a9a-e1f8-4b40-aaf9-27da269a2738', 'app', '用户端', 'nacos', 1699935354326, 1699935354326);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('nacos', '$2a$10$EuWPZHzz32dJN7jexM34MOeYirDdFAZm2kuWj7VEOJhhZkDrxfvUu', 1);

SET FOREIGN_KEY_CHECKS = 1;
