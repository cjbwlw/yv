/*
 Navicat Premium Data Transfer

 Source Server         : 本机
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : localhost:3306
 Source Schema         : yv_app

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 22/11/2023 12:36:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for apply_friend
-- ----------------------------
DROP TABLE IF EXISTS `apply_friend`;
CREATE TABLE `apply_friend`  (
  `id` bigint(20) NOT NULL,
  `u_id` bigint(20) NULL DEFAULT NULL COMMENT '申请用户id',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '申请用户名称',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '申请用户头像',
  `f_id` bigint(20) NULL DEFAULT NULL COMMENT '被申请人id',
  `status` tinyint(4) NULL DEFAULT 0 COMMENT '状态，0待申请，1通过，2拒绝',
  `time` datetime NULL DEFAULT NULL COMMENT '申请时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of apply_friend
-- ----------------------------

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` bigint(20) NOT NULL,
  `f_id` bigint(20) NULL DEFAULT NULL COMMENT '朋友圈id',
  `u_id` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '评论内容',
  `release_time` datetime NULL DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comment
-- ----------------------------

-- ----------------------------
-- Table structure for friends_circle
-- ----------------------------
DROP TABLE IF EXISTS `friends_circle`;
CREATE TABLE `friends_circle`  (
  `id` bigint(20) NOT NULL,
  `u_id` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '朋友圈内容',
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片列表，用\",\"分隔',
  `release_time` datetime NULL DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of friends_circle
-- ----------------------------

-- ----------------------------
-- Table structure for friends_circle_like
-- ----------------------------
DROP TABLE IF EXISTS `friends_circle_like`;
CREATE TABLE `friends_circle_like`  (
  `u_id` bigint(20) NOT NULL COMMENT '用户id',
  `f_id` bigint(20) NOT NULL COMMENT '朋友圈id'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of friends_circle_like
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
