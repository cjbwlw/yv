import { createRouter, createWebHistory } from "vue-router";
import routes from "./routes";
import { useSysMenuStore } from "@/stores/system/menu";
import pinia from "@/stores";
import { ElMessage } from "element-plus";

const router = createRouter({
  history: createWebHistory(),
  routes,
});

// 路由守卫
router.beforeEach((to, from, next) => {
  console.log(to, from);
  const sysStore = useSysMenuStore(pinia);
  let user: any = localStorage.getItem("user");
  user = JSON.parse(user);
  let token;
  if (user) {
    token = user.token;
  }
  if (to.path == "/login") {
    if (!token) {
      next();
    } else {
      ElMessage.info("您已登录");
      next({ path: "/" });
    }
  } else if (!token) {
    ElMessage.info("您还没有登录");
    next({ path: "/login" });
  } else if (!sysStore.hasRoute) {
    sysStore.getRouter().then((data) => {
      data.forEach((r) => {
        router.addRoute(r);
      });
    });
    sysStore.changeRouteStatus(true);
    next({ path: to.path });
  } else {
    console.log("已经有路由了------------", routes);
    next();
  }
});

// 跳转路由之后执行
router.afterEach((to, form) => {
  console.log(to, form);
});

export default router;
