import { RouteRecordRaw } from 'vue-router'
import Home from '@/pages/index.vue'


const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'index',
        component: Home,
        children: [
            {
                path: '',
                name: 'index',
                component: ()=>import("@/pages/index/index.vue"),
            }
        ]
    },
    {
        path: '/login',
        name: 'login',
        component: ()=>import("@/pages/login/index.vue"),
    },
    {
        path: '/system',
        name: 'home',
        component: Home,
        children: [{
            path: 'userInfo',
            name: 'userInfo',
            component: ()=>import("@/pages/system/user/info.vue"),
        }]
    },
    {
        path: '/404',
        name: '404页面',
        component: () => import("@/pages/404/index.vue")
    },
    {
        //对找不到路由进行重定向
       path: '/:catchAll(.*)',
       redirect: '/'   
    //    redirect: '/404'
    }
]

export default routes