import * as system from '@/api/service/system' // 系统模块接口
import * as sso from '@/api/service/sso' // 认证中心模块接口
import * as chat from '@/api/service/chat' // 认证中心模块接口

export default {
  ...system,
  ...sso,
  ...chat

}