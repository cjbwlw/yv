import api from '@/utils/request-ts'

// 好友申请
export const getApplyFriendList = (data:any) => {
    return api.post<any>("/app/applyFriend/list",data);
}

export const delApplyFriendById = (id:any) => {
    return api.delete<any>(`/app/applyFriend/${id}`);
}


// 评论
export const getCommentList = (data:any) => {
    return api.post<any>("/app/comment/list",data);
}

export const delCommentById = (id:any) => {
    return api.delete<any>(`/app/comment/${id}`);
}

// 朋友圈
export const getFriendsCircleList = (data:any) => {
    return api.post<any>("/app/friendCircle/list",data);
}

export const delFriendsCircleById = (id:any) => {
    return api.delete<any>(`/app/friendCircle/${id}`);
}