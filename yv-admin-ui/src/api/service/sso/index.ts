import { LoginUser } from "@/type/system";
import api from "@/utils/request-ts";

// 登录
export const login = (data: LoginUser) => {
  return api.post(`/sso/login`, data);
};

// 认证token
export const authToken = (data: string) => {
  return api.post<boolean>(`/sso/authToken`, data);
};
