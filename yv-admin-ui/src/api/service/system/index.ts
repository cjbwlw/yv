import api from '@/utils/request-ts'
import {QueryMenu,MenuDTO, RoleDTO, QueryRole, QueryUser, UserDTO} from '@/type/system'

// return request({
//     url: `/system/menu/list`,
//     method: 'GET',
//     params:queryMenu
// })

/** -------------菜单---------------- */

// 获取所有菜单列表，不递归构建子菜单
export const getAllMenu = () => {
    return api.get<any>("/system/menu/allList");
}

// 获取菜单列表
export const getMenuList = (queryMenu:QueryMenu) => {
    return api.get<any>("/system/menu/list",queryMenu)
}

// 修改菜单状态
export const changeMenuStatus = (id:string,status:string) => {
    return api.put<bigint>(`/system/menu/status/${status}`,{id})
}

// 添加菜单
export const addMenu = (data:MenuDTO) => {
    return api.post<any>(`/system/menu`,data)
}

// 修改菜单
export const changeMenu = (data:MenuDTO) => {
    return api.put<any>(`/system/menu`,data)
}

// 删除菜单
export const removeMenu = (data:string[]) => {
    return api.delete<any>(`/system/menu`,data)
}

/** -------------用户---------------- */

export const getvisualData = () => {
    return api.get<any>(`/system/user/visualData`)
}


// 根据id获取用户信息
export const getUserById = (id:string) => {
    return api.get<any>(`/system/user/${id}`,1)
}

// 查询用户列表
export const getUserList = (queryUser:QueryUser) => {
    return api.get<any>(`/system/user/list`,queryUser)
}

// 添加用户
export const addUser = (data:UserDTO) => {
    return api.post<any>(`/system/user`,data)
}

export const removeUser = (data:string[]) => {
    return api.delete<any>(`/system/user`,data)
}
// 修改用户状态
export const changeUserStatus = (id:string,status:string) => {
    return api.put<bigint>(`/system/user/status/${status}`,{id})
}

export const changeUser = (data:UserDTO) => {
    return api.put<any>(`/system/user`,data)
}

/** -------------角色---------------- */

export const getRoleList = (queryRole:QueryRole) => {
    return api.get<any>(`/system/role/list`,queryRole)
}

export const changeRole = (data:RoleDTO) => {
    return api.put<any>(`/system/role`,data)
}

export const removeRole = (data:string[]) => {
    return api.delete<any>(`/system/role`,data)
}
// 添加角色
export const addRole = (data:RoleDTO) => {
    return api.post<any>(`/system/role`,data)
}

// 获取所有角色列表
export const getAllRole = () => {
    return api.get<any>("/system/role/allList");
}