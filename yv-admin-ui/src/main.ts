import { createApp } from 'vue'
import App from './App.vue'
// 清除默认样式
import "@/assets/scss/common/reset.scss";
// 全局样式
import "@/assets/scss/index.scss";
// 暗黑模式
import 'element-plus/theme-chalk/dark/css-vars.css';
import '@/assets/scss/dark/index.scss';

// 饿了么前端框架
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import zhCn from "element-plus/es/locale/lang/zh-cn";
// 所有业务api接口
import api from './api'
// pinia仓库
import pinia from '@/stores'
// 路由
import router from "@/router"
import * as echarts from 'echarts';

// 创建实例
const app = createApp(App)
// 注册全局api方法
app.config.globalProperties.$api = api
// 全局引入elementIcon图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
// 国际化
app.use(ElementPlus, {
    locale: zhCn,
  })
// 路由
app.use(router)
// 仓库
app.use(pinia)
// echarts
app.config.globalProperties.$echarts = echarts;
app.mount('#app')
console.log(

"  ██    ██ ██      ██  \n"+
"  ░░██  ██ ░██     ░██ \n"+
"   ░░████  ░██     ░██ \n"+
"    ░░██   ░░██    ██  \n"+
"     ░██    ░░██  ██   \n"+
"     ░██     ░░████    \n"+
"     ░██      ░░██     \n"+
"     ░░        ░░      \n"
)

console.log("项目启动成功")
