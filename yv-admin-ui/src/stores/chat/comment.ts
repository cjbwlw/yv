import { defineStore } from "pinia";
import api from "@/api";
import { reactive } from "vue";
import { ElMessage,ElMessageBox } from "element-plus";

export const useComment = defineStore("comment", {
  state: () => {
    return {
        searchParame:{
            content:"",
            total:0
        },
        data:reactive<any[]>([]),
        info:reactive({
            user: {
                nickname: "",
                avatar:""
            },
            friendsCircleDTO:{
                content:""
            },
            content: "",
            releaseTime: ""
        })
    };
  },
  actions: {
    getList(){
        api.getCommentList(this.searchParame).then((data:any)=>{
            console.log(data)
            this.data = data.list
            this.searchParame.total = parseInt(data.total);
        })
    },
    removeByID(id:any){
        ElMessageBox.confirm("确定删除？","警告",{
            confirmButtonText:"确定",
            cancelButtonText:"取消",
            icon:"warning"
        }).then(()=>{
            api.delCommentById(id).then((data)=>{
                console.log(data)
                ElMessage.success("删除成功")
                this.getList()
            })
        }).catch(()=>{})
    }
  },
  getters: {},
});
