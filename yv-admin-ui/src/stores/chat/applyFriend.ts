import { defineStore } from "pinia";
import api from "@/api";
import { reactive } from "vue";
import { ElMessage,ElMessageBox } from "element-plus";

export const useApplyFriend = defineStore("applyFriend", {
  state: () => {
    return {
        searchParame:{
            name:"",
            status: -1,
            total:0
        },
        data:reactive<any[]>([]),
        info:reactive({
            fu: {
                nickname: "",
                avatar:""
            },
            name:"",
            avatar:"",
            time: ""
        })
    };
  },
  actions: {
    getList(){
        api.getApplyFriendList(this.searchParame).then((data:any)=>{
            console.log(data)
            this.data = data.list
            this.searchParame.total = parseInt(data.total);
        })
    },
    removeByID(id:any){
        ElMessageBox.confirm("确定删除？","警告",{
            confirmButtonText:"确定",
            cancelButtonText:"取消",
            icon:"warning"
        }).then(()=>{
            api.delApplyFriendById(id).then((data)=>{
                console.log(data)
                ElMessage.success("删除成功")
                this.getList()
            })
        }).catch(()=>{})
    }
  },
  getters: {},
});
