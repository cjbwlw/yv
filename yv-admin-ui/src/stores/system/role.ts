import { defineStore } from "pinia";
import api from "@/api";
import { RoleDTO, QueryRole } from "@/type/system";
import { reactive } from "vue";
import { ElMessage } from "element-plus";

export const useSysRoleStore = defineStore("role", {
  state: () => {
    return {
      // 查询参数
      queryRole: reactive<QueryRole>({
        name: "",
        authority:"",
        num: 1,
        size: 7,
        total: 0,
      }),
      roleList: reactive<RoleDTO[]>([]),
      //所有角色
      allRole: reactive<RoleDTO[]>([]),
      pretreatmentRole: reactive<RoleDTO>({
        id: "",
        name: "",
        authority: "",
      }),
      roleIds: reactive<string[]>([]),
    };
  },
  actions: {
    getAllList(){
      api.getAllRole().then((data)=>{
        this.allRole = data
      })
    },
    addRole() {
        api.addRole(this.pretreatmentRole).then(() => {
          ElMessage.success("添加菜单成功");
          this.getList();
        });
      },
    removeRole() {
      api.removeRole(this.roleIds).then(() => {
        ElMessage.success("删除菜单成功");
        this.getList();
        this.roleIds = []
      });
    },
    changeRole() {
      api.changeRole(this.pretreatmentRole).then(() => {
        ElMessage.success("修改菜单成功");
        this.getList();
      });
    },
    getList() {
      api.getRoleList(this.queryRole).then((data) => {
        this.roleList = data.list;
        this.queryRole.total = parseInt(data.total);
      });
    },
  },
  getters: {},
});
