import { defineStore } from "pinia";
import { QueryMenu, Menu, MenuDTO } from "@/type/system";
import { reactive, ref } from "vue";
import api from "@/api";
import { RouteRecordRaw } from "vue-router";
import { setSubmenu, menuToRouter } from "@/utils/router";
import { ElMessage } from "element-plus";

// useStore 可以是 useUser、useCart 之类的任何东西
// 第一个参数是应用程序中 store 的唯一 id
export const useSysMenuStore = defineStore("menu", {
  // 推荐使用 完整类型推断的箭头函数
  //数据
  state: () => {
    return {
      // 查询参数
      queryMenu: reactive<QueryMenu>({
        title: "",
        status: "",
        num: 1,
        size: 7,
        total: 0
      }),
      /**
       * 新增修改菜单
       */
      pretreatmentMenu: reactive<MenuDTO>({
        id: "请选择父菜单",
        name: "",
        title: "",
        pid: "",
        icon: "请选择图标",
        url: "",
        path: "",
        component: "",
        status: "1",
      }),
      // 菜单列表
      menuList: reactive<Menu[]>([]),
      // 菜单路由
      routes: reactive<any>([]),
      // 是否有路由
      hasRoute: ref(false),
      // 是否关闭菜单栏
      isCollapse: ref(false),
      menuIds: reactive<string[]>([]),
      allMenu: reactive<MenuDTO[]>([])
    };
  },
  //类似计算属性
  getters: {
    // doubleCount: (state) => state.counter * 2,
  },
  //方法
  actions: {
    /** 
     * 获取所有菜单
     */
    getAllMenu() {
      api.getAllMenu().then((data) => {
        this.allMenu = data;
        console.log(this.allMenu)
      })
    },
    /**
     * 删除菜单
     */
    removeMenu() {
      console.log(this.menuIds)
      api.removeMenu(this.menuIds).then(() => {
        // 刷新
        this.getList();
        this.getRouter();
        this.menuIds = []; //删除之后置空
        ElMessage.success("删除菜单成功");
      });
    },
    /**
     * 修改菜单
     */
    changeMenu() {
      api.changeMenu(this.pretreatmentMenu).then(() => {
        // 刷新
        this.getList();
        this.getRouter();
        ElMessage.success("修改菜单成功");
      });
    },
    /**
     * 添加菜单
     */
    addMenu() {
      api.addMenu(this.pretreatmentMenu).then(() => {
        // 刷新
        this.getList();
        this.getRouter();
        ElMessage.success("添加菜单成功");
      });
    },
    /**
     * 修改菜单状态
     */
    changeMenuStatus(id: string, status: string,message:string) {
      return api.changeMenuStatus(id, status).then(() => {
        ElMessage.success(`${message}成功`)
        // 刷新
        this.getList();
        this.getRouter();
      })
    },
    /**
     * 查询菜单列表
     */
    getList() {
      api.getMenuList(this.queryMenu).then((data) => {
        console.log("查询菜单列表", data);
        this.menuList = data.list;
        this.queryMenu.total = parseInt(data.total);
      });
    },

    /**
     * 获取所有菜单列表并展示
     * @returns 返回Promise<page`>
     */
    async getRouter() {
      let routeList: RouteRecordRaw[] = [];
      await api
        .getMenuList({
          title: "",
          status: 1,
          num: 0,
          size: 0,
          total: 0,
        })
        .then((data) => {
          console.log("查询有效菜单列表", data.list);
          this.routes = data.list;
          data.list.forEach((menu: Menu) => {
            let r: RouteRecordRaw;
            if (menu.submenu) {
              r = setSubmenu(menu, menu.submenu);
            } else {
              r = menuToRouter(menu);
            }
            console.log("添加路由", r);
            routeList.push(r);
          });
        });
      return routeList;
    },
    setRoutes(list: any) {
      this.routes = list;
    },
    changeRouteStatus(is: boolean) {
      this.hasRoute = is;
    },
  },
});
