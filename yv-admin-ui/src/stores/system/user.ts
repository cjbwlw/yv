import { defineStore } from "pinia";
import api from "@/api";
import { UserDTO, QueryUser } from "@/type/system";
import { reactive } from "vue";
import { ElMessage, ElNotification } from "element-plus";
import { Router } from "vue-router";

export const useSysUserStore = defineStore("user", {
  state: () => {
    return {
      // 查询参数
      queryUser: reactive<QueryUser>({
        name: "",
        nickname: "",
        status: "",
        num: 1,
        size: 7,
        total: 0,
      }),
      pretreatmentUser: reactive<UserDTO>({
        id: "",
        rid: "",
        roleName:"",
        name: "",
        avatar: "",
        nickname: "",
        gender: "1",
        idNo: "",
        phone: "",
        email: "",
        status: "1",
        token: ""
      }),
      userList: reactive<UserDTO[]>([]),
      userIds: reactive<string[]>([]),
      loginUser: reactive({
        username: "",
        password: "",
      }),
      userInfo: reactive<any>({}),
    };
  },
  actions: {
    /** 
     * 根据id获取用户
     */
    getById(id:string){
      api.getUserById(id).then((data)=>{
        console.log(data)
        const {id, rid, avatar, name, nickname, gender, idNo, phone, email, roleName,token} = data;
        const user = {id, rid, avatar, name, nickname, gender, idNo, phone, email, roleName,token}
        user.token = JSON.parse(localStorage.getItem("user")||"").token
        this.userInfo = user
        localStorage.setItem("user",JSON.stringify(user))
      })
    },
    /**
     * 用户登录
     */
    login(router: Router) {
      api.login(this.loginUser).then((data: any) => {
        if(data.id!="1"){
          ElMessage.error("您不是管理员，请退出系统！！！")
          return
        }
        ElNotification({
          title: "登录成功",
          message: `登录系统成功，欢迎"${data.nickname}"`,
          type: "success",
          duration: 3000,
        });
        const { id, rid, avatar, name, nickname, gender, idNo, phone, email, roleName, token } = data;
        this.userInfo = { id, rid, avatar, name, nickname, gender, idNo, phone, email, roleName, token };
        localStorage.setItem("user", JSON.stringify(this.userInfo));
        router.push({
          path: "/",
        });
      });
    },
    /**
     * 修改用户状态
     */
    changeUserStatus(id: string, status: string, message: string) {
      return api.changeUserStatus(id, status).then(() => {
        ElMessage.success(`${message}成功`);
        // 刷新
        this.getList();
      });
    },
    addUser() {
      api.addUser(this.pretreatmentUser).then(() => {
        ElMessage.success("添加用户成功");
        this.getList();
      });
    },
    changeUser() {
      api.changeUser(this.pretreatmentUser).then(() => {
        ElMessage.success("修改用户成功");
        this.userInfo.value = this.pretreatmentUser
        this.getList();
      });
    },
    removeRole() {
      api.removeUser(this.userIds).then(() => {
        ElMessage.success("删除用户成功");
        this.getList();
        this.userIds = [];
      });
    },
    getList() {
      api.getUserList(this.queryUser).then((data) => {
        this.userList = data.list;
        this.queryUser.total = parseInt(data.total);
      });
    },
  }
});
