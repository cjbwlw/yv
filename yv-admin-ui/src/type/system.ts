// 查询超类
export interface Query {
  num: number;
  size: number;
  total: number;
}

export interface LoginUser {
  username: string;
  password: string;
}

export interface QueryUser extends Query {
  name: string;
  nickname: string;
  status: string;
}

// 角色查询
export interface QueryRole extends Query {
  name: string;
  authority: string;
}

// 菜单查询
export interface QueryMenu extends Query {
  title: string;
  status: number | string;
}

export interface RoleDTO {
  id: string;
  name: string;
  authority: string;
}

export interface UserDTO {
  id: string;

  /** 角色id */
  rid:string;

  /** 角色名称 */
  roleName:string;

  /** 账号 */
  name: string;

  /** 头像url */
  avatar: string;

  /** 昵称 */
  nickname: string;

  /** 性别，1男，0女 */
  gender: string;

  /** 身份证号码 */
  idNo: string;

  /** 手机号码 */
  phone: string;

  /** 邮箱 */
  email: string;

  /** 状态，1可用，0禁用 */
  status: string;

  /** token */
  token: string;
}

export interface MenuDTO {
  /** 主键 */
  id: string;
  /** 名称 */
  name: string;

  /** 菜单标题 */
  title: string;

  /** 父id */
  pid: string;

  /** 图标 */
  icon: string;

  /** 前端全路由 */
  url: string;

  /** 前端路由 */
  path: string;

  /** 组件路径 */
  component: string;

  /** 状态 */
  status: number | string;
}

export interface Menu {
  /** 主键 */
  id: string;

  /** 名称 */
  name: string;

  /** 菜单标题 */
  title: string;

  /** 父id */
  pID: string;

  /** 图标 */
  icon: string;

  /** 前端全路由 */
  url: string;

  /** 前端路由 */
  path: string;

  /** 组件路径 */
  component: string;

  /** 状态 */
  status: string;

  /** 逻辑删除 */
  deleted: string;

  /** 创建时间 */
  createTime: string;

  /** 创建用户 */
  createUser: string;

  /** 更新时间 */
  updateTime: string;

  /** 更新用户 */
  updateUser: string;

  /** 子菜单 */
  submenu: Menu[];
}
