type Result<T> = {
    code: number,
    message: string,
    result: T
}

type loginType = {
    token: string;
};

type page = {
    list:[],
    total:number
}