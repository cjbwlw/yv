import { RouteRecordRaw } from "vue-router";
import { Menu } from "@/type/system";

const modules = import.meta.glob("../pages/**/*.vue");

/**
 * 把菜单转换为路由类型
 * @param m
 * @returns
 */
export const menuToRouter = (m: Menu) => {
  let route: RouteRecordRaw;
  route = {
    name: m.name,
    path: m.path,
    meta: {
      icon: m.icon,
      title: m.title,
    },
    component: modules["../pages/" + m.component],
    children: [],
  };
  return route;
};

/**
 * 设置子菜单,第一次进入一定有子菜单
 * @param menu
 * @param data
 */
export const setSubmenu = (menu: Menu, submenu: Menu[]) => {
  const parentMenu: RouteRecordRaw = menuToRouter(menu);

  //递归设置子菜单
  submenu.forEach((m) => {
    parentMenu.children.push(setSubmenu(m, m.submenu));
  });

  return parentMenu;
};
