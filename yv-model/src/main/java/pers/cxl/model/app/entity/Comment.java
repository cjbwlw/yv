package pers.cxl.model.app.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 评论表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Comment {

    private Long id;

    /** 用户id */
    @TableField("u_id")
    private Long uid;

    /** 评论的朋友圈id */
    @TableField("f_id")
    private Long fid;

    /** 评论内容 */
    private String content;

    /** 发布时间 */
    @TableField("release_time")
    private LocalDateTime releaseTime;


}
