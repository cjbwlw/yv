package pers.cxl.model.app.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import pers.cxl.model.system.dto.QueryDTO;
import pers.cxl.model.system.dto.UserDTO;

import java.time.LocalDateTime;

/**
 * 评论表
 */
@Data
@Accessors(chain = true)
public class CommentDTO extends QueryDTO {

    private Long id;

    /** 用户id */
    private Long uid;

    /** 评论的朋友圈id */
    private Long fid;

    /** 评论内容 */
    private String content;

    /** 发布时间 */
    @TableField("release_time")
    private LocalDateTime releaseTime;

    FriendsCircleDTO friendsCircleDTO;

    private UserDTO user;


}
