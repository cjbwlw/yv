package pers.cxl.model.app.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import pers.cxl.model.app.entity.Comment;
import pers.cxl.model.system.dto.QueryDTO;
import pers.cxl.model.system.dto.UserDTO;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 朋友圈
 */
@Data
@Accessors(chain = true)
public class FriendsCircleDTO extends QueryDTO {

    private Long id;

    /** 用户id */
    private Long uid;

    /** 所发朋友圈用户信息 */
    private UserDTO user;

    /** 朋友圈内容 */
    private String content;

    /** 图片列表，用","分隔 */
    private String img;

    /** 发布时间 */
    private LocalDateTime releaseTime;

    /** 我对当前朋友圈的点赞状态 */
    private Boolean like;

    /** 当前朋友圈的喜欢列表 */
    private StringBuilder likes;

    /** 当前朋友圈的评论列表 */
    private List<CommentDTO> comments;

}
