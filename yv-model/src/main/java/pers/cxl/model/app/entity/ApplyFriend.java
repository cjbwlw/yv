package pers.cxl.model.app.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import pers.cxl.model.system.dto.QueryDTO;
import pers.cxl.model.system.dto.UserDTO;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ApplyFriend extends QueryDTO {

    private Long id;

    @TableField("u_id")
    private Long uid;

    private String name;

    private String avatar;

    @TableField("f_id")
    private Long fid;

    private Integer status;

    private LocalDateTime time;

    /** 被申请用户 */
    @TableField(exist = false)
    private UserDTO fu;

}
