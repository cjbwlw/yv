package pers.cxl.model.app.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 朋友圈
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class FriendsCircle {

    private Long id;

    /** 用户id */
    @TableField("u_id")
    private Long uid;

    /** 朋友圈内容 */
    private String content;

    /** 图片列表，用","分隔 */
    private String img;

    /** 发布时间 */
    @TableField("release_time")
    private LocalDateTime releaseTime;
}
