package pers.cxl.model.app.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 朋友圈点赞
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FriendsCircleLike {

    /** 用户id:对fid点赞的用户id */
    @TableField("u_id")
    private Long uid;

    /** 朋友圈id */
    @TableField("f_id")
    private Long fid;

}
