package pers.cxl.model.system.dto;

import lombok.Data;

@Data
public class RegisterUserDTO {

    private String avatar;
    private String nickname;
    private String password;


}
