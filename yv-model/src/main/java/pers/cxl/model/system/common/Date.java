package pers.cxl.model.system.common;

import java.text.SimpleDateFormat;

public class Date {

    public String toString(String matching) {
        switch (matching){
            case "yyyy":
            case "MM":
            case "dd":
            case "HH":
            case "mm":
            case "ss":
                break;
            default:
                throw new RuntimeException("只能匹配yyyy,MM,dd,HH,mm,ss");
        }
        SimpleDateFormat sdf = new SimpleDateFormat(matching);
        return sdf.format(new java.util.Date());
    }

}
