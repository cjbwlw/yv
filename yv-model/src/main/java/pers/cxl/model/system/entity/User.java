package pers.cxl.model.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /** 角色id */
    private Long rid;

     /** 账号 */
    private String name;

     /** 头像url */
    private String avatar;

    /** 昵称 */
    private String nickname;

    /** 登录密码 */
    private String password;

    /** 登录IP */
    private String ip;

    /** 登录时间 */
    private LocalDateTime loginTime;

    /** 性别，1男，0女 */
    private Integer gender;

    /** 身份证号码 */
    private String idNo;

    /** 手机号码 */
    private String phone;

    /** 邮箱 */
    private String email;

    /** 状态，1可用，0禁用 */
    private Integer status;

    /** 0未删除，1删除 */
    @TableLogic
    private Integer deleted;

    /** 注册时间 */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime registerTime;

}
