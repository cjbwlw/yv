package pers.cxl.model.system.dto;

import lombok.Data;

@Data
public class QueryMenuDTO extends QueryDTO {

    /** 标题 */
    private String title;

    /** 名称 */
    private Integer status;

}
