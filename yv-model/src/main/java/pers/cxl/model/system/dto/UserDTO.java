package pers.cxl.model.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 用户类
 */
@Data
public class UserDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /** 角色id */
    private Long rid;

     /** 账号 */
    private String name;

     /** 头像url */
    private String avatar;

    /** 昵称 */
    private String nickname;

    /** 登录IP */
    private String ip;

    /** 登录时间 */
    private LocalDateTime loginTime;

    /** 性别，1男，0女 */
    private Integer gender;

    /** 身份证号码 */
    private String idNo;

    /** 手机号码 */
    private String phone;

    /** 邮箱 */
    private String email;

    /** 状态，1可用，0禁用 */
    private Integer status;

    /** 0未删除，1删除 */
    private Integer deleted;

    /** 注册时间 */
    private LocalDateTime registerTime;

    /** 角色名称 */
    private String roleName;

    /** token认证 */
    private String token;

    /** 是否是好友 */
    private Boolean isFriend;
}
