package pers.cxl.model.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 菜单类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 名称 */
    private String name;

    /** 菜单名称 */
    private String title;

    /** 父id */
    @TableField("p_id")
    private Long pid;

    /** 图标 */
    private String icon;

    /** 前端全路由 */
    private String url;

    /** 前端路由 */
    private String path;

    /** 组件路径 */
    private String component;

    /** 状态 */
    private Integer status;

    /** 逻辑删除 */
    @TableLogic
    private Integer deleted;

    /** 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /** 创建用户 */
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;

    /** 更新时间 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /** 更新用户 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;

}
