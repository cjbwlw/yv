package pers.cxl.model.system.vo;


import lombok.Data;

import java.io.Serializable;

@Data
public class MenuVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 名称 */
    private String name;

    /** 菜单名称 */
    private String title;

    /** 父id */
    private String pID;

    /** 图标 */
    private String icon;

    /** 前端全路由 */
    private String url;

    /** 前端路由 */
    private String path;

    /** 组件路径 */
    private String component;

    /** 状态 */
    private String status;

}
