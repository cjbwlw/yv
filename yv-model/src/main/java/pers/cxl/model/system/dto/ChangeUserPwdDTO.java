package pers.cxl.model.system.dto;

import lombok.Data;

@Data
public class ChangeUserPwdDTO {

    private Long id;
    private String oldPwd;
    private String newPwd;

}
