package pers.cxl.model.system.dto;

import lombok.Data;

@Data
public class QueryRoleDTO  extends QueryDTO {

    /** 角色名称 */
    private String name;
    private String authority;

}
