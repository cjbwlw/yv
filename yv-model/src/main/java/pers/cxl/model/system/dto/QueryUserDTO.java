package pers.cxl.model.system.dto;

import lombok.Data;

@Data
public class QueryUserDTO extends QueryDTO {

    /** 账号 */
    private String name;

    /** 昵称 */
    private String nickname;

    /** 状态 */
    private Integer status;

}
