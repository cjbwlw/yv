package pers.cxl.model.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Data
public class MenuDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 名称 */
    private String name;

    /** 菜单名称 */
    private String title;

    /** 父id */
    private Long pid;

    /** 图标 */
    private String icon;

    /** 前端全路由 */
    private String url;

    /** 前端路由 */
    private String path;

    /** 组件路径 */
    private String component;

    /** 状态 */
    private Integer status;

    /** 逻辑删除 */
    private Integer deleted;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime createTime;

    /** 创建用户 */
    private String createUser;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime updateTime;

    /** 更新用户 */
    private String updateUser;

    /** 子菜单 */
    private List<MenuDTO> submenu;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuDTO menuDTO = (MenuDTO) o;
        return Objects.equals(id, menuDTO.id) &&
                Objects.equals(name, menuDTO.name) &&
                Objects.equals(title, menuDTO.title) &&
                Objects.equals(pid, menuDTO.pid) &&
                Objects.equals(icon, menuDTO.icon) &&
                Objects.equals(url, menuDTO.url) &&
                Objects.equals(path, menuDTO.path) &&
                Objects.equals(component, menuDTO.component) &&
                Objects.equals(status, menuDTO.status) &&
                Objects.equals(deleted, menuDTO.deleted) &&
                Objects.equals(createTime, menuDTO.createTime) &&
                Objects.equals(createUser, menuDTO.createUser) &&
                Objects.equals(updateTime, menuDTO.updateTime) &&
                Objects.equals(updateUser, menuDTO.updateUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, title, pid, icon, url, path, component, status, deleted, createTime, createUser, updateTime, updateUser);
    }
}
