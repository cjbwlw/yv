package pers.cxl.model.system.dto;

import lombok.Data;

@Data
public class LoginUserDTO {

    private String username;
    private String password;
    private String token;


}
