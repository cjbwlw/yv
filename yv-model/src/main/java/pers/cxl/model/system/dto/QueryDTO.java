package pers.cxl.model.system.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public abstract class QueryDTO {

    /** 当前页 */
    @TableField(exist = false)
    private int num;
    /** 每页数量 */
    @TableField(exist = false)
    private int size;
    /** 总数 */
    @TableField(exist = false)
    private int total;
    /** 模糊查询sql起始数 */
    @TableField(exist = false)
    private int start;

}
