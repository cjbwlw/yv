export default {
    config: {
        baseURL: 'https://8v249k2561.goho.co',
        getToken() {
            let token = uni.getStorageSync('info').token;
			console.log("登录凭证",token)
            if (!token) {
                return uni.reLaunch({
                    url: '/pages/login/login'
                })
            }
            return token
        },
        // 请求拦截器
        beforeRequest(options = {}) {
            return new Promise((resolve, reject) => {
                options.url = this.baseURL + options.url;
                options.method = options.method || 'GET';
                options.header = {
                    "Token": this.getToken()
                }
                resolve(options)
            })
        },
        // 响应拦截器
        handleResponse(data){
            return new Promise((resolve, reject) => {
                const [err, res] = data;
				console.log("成功数据",res)
				console.log("失败数据",err)
                if (res && res.statusCode !== 200) {
                    let msg = res.data.msg || '请求错误';
                    uni.showToast({
                        icon: 'none',
                        title: msg
                    })
                    return reject(msg)
                }else{
					// 判断服务器自定义错误
					const code =  res.data.code;
					if(code>=5000){
						uni.showToast({
						    icon: 'none',
						    title: res.data.message
						})
						return reject(msg)
					}
				}
                if (err) {
                    uni.showToast({
                        icon: 'none',
                        title: '请求错误'
                    })
                    return reject(err)
                }
                return resolve(res.data.data)
            })
        },
    },
    request(options = {}) {
        return this.config.beforeRequest(options).then(opt => {
            return uni.request(opt)
        }).then(res => this.config.handleResponse(res))
    }
}
