import api from './request'

// 获取朋友列表
export function list(id) {
    return api.request({
        method: 'post',
        url: `/user/app/friend/list/${id}`
    })
}

// 申请添加好友
export function applyFriend(data) {
    return api.request({
        method: 'post',
        url: `/user/app/friend/apply/friend`,
		data
    })
}

// 查询申请列表
export function applyList(id) {
    return api.request({
        method: 'post',
        url: `/user/app/friend/apply/list/${id}`
    })
}

// 处理申请列表状态
export function applyChange(data) {
    return api.request({
        method: 'post',
        url: `/user/app/friend/apply/change`,
		data
    })
}

// 根据账号查询用户
export function getByAccount(name) {
    return api.request({
        method: 'post',
        url: `/user/app/friend/search/${name}`
    })
}
