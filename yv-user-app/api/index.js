export default function(url, data, method) {
	const BASE_URL = "http://192.168.1.102:80"
	return new Promise(function(resolve, reject) {
		uni.request({
			url: BASE_URL + url,
			data,
			method,
			success(res) {
				console.log("请求成功", res)
				const code = res.statusCode
				// 判断状态码
				// switch (code) {
				// 	case 401:
				// 		uni.reLaunch({
				// 			url: "/pages/login/login"
				// 		})
				// 		break;
				// }
				resolve(res)
			},
			fail(err) {
				console.log("请求失败", err)
				reject(err)
			}
		})
	})
}