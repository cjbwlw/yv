import api from './request'

// 用户登录
export function list(id) {
    return api.request({
        method: 'post',
        url: `/user/app/friendsCircle/list/${id}`
    })
}

// 用户朋友圈点赞
export function like(flag,data) {
    return api.request({
        method: 'put',
        url: `/user/app/friendsCircle/like/${flag}`,
		data
    })
}

// 用户朋友圈评论
export function comment(data) {
    return api.request({
        method: 'put',
        url: `/user/app/friendsCircle/comment`,
		data
    })
}

export function send(data) {
    return api.request({
        method: 'put',
        url: `/user/app/friendsCircle/send`,
		data
    })
}