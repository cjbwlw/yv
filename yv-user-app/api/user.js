import api from './request'

// 用户登录
function login(data) {
    return api.request({
        method: 'post',
        url: '/api/sso/login',
        data: data
    })
}

// 用户注册
function register(data) {
    return api.request({
        method: 'post',
        url: '/user/app/user/registry',
        data: data
    })
}

function getUserById(id) {
    return api.request({
        method: 'get',
        url: `/user/app/user/${id}`,
    })
}

function chengeUserById(user) {
    return api.request({
        method: 'put',
        url: `/api/system/user`,
		data: user
    })
}

function chengeUserAvatar(avatar) {
    return api.request({
        method: 'post',
        url: `/api/system/file/upload`,
		data: avatar
    })
}

function changePwd(data) {
    return api.request({
        method: 'put',
        url: `/api/system/user/changePwd`,
		data: data
    })
}

export {
	login,
	register,
	getUserById,
	chengeUserById,
	chengeUserAvatar,
	changePwd
}