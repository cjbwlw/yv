import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	...App
})


app.$mount()

// // 拦截器
// uni.addInterceptor('request', {
// 	invoke(args) {
// 		// request 触发前判断是否有token
// 		console.log("请求信息",args)
// 		if(args.url == "/api/sso/login"){
// 			return
// 		}
// 		let token = ""
// 		uni.getStorage({
// 			key: "token",
// 			success(res) {
// 				console.log("获取token成功", res)
// 				token = res.data
// 			},
// 			fail(err) {
// 				console.log("获取token失败", err)
// 				uni.showToast({
// 					title: "登录失效",
// 					icon: "error",
// 					duration: 500
// 				})
// 				setTimeout(() => {
// 					uni.reLaunch({
// 						url: "/pages/login/login"
// 					})
// 				}, 500)
// 			}
// 		})
// 		args.header = {
// 			...args.header,
// 			token
// 		}
// 	},
// })