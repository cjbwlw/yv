package pers.cxl.common.constant;

/**
 * 常量消息
 */
public interface MessageConstant {

    String UNKNOWN_USER = "未知用户";
    String  UNKNOWN_IP = "未知IP";
    String  LOGIN_ERR = "登陆失败";
    String  REGISTER_ERR = "注册失败";
    String  NO_FILE = "未找到文件，请先上传文件";
    String  UPLOAD_ERR = "文件上传失败";
    String  DOWNLOAD_ERR = "文件下载失败";
    String  PASSWORD_ERR = "密码错误";
    String  GET_FRIEND_LIST_ERR = "获取好友列表失败";

    String SERVER_ERR = "服务器内部错误";
    String RPC_ERR = "远程调用失败";
    String ACCOUNT_FORBIDDEN = "账号已被禁用";
}
