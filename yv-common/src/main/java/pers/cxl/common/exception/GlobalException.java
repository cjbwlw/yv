package pers.cxl.common.exception;

import lombok.Getter;

/**
 * 全局异常
 */
@Getter
public class GlobalException extends RuntimeException {

    public GlobalException(String message) {
        super(message);
    }
}
