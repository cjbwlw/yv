package pers.cxl.common.config;

import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 接口文档配置
 */
@Configuration
@EnableSwagger2
public class Knife4jConfig {

    @Bean
    public Docket webApiConfig() {
        return new Docket(DocumentationType.SWAGGER_2)
                // 创建接口文档的具体信息
                .apiInfo(webApiInfo())
                .groupName("系统管理")
                // 创建选择器，控制哪些接口被加入文档
                .select()
                // 指定@ApiOperation标注的接口被加入文档
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .build();
    }

    private ApiInfo webApiInfo() {
        return new ApiInfoBuilder()
                // 文档标题
                .title("YV")
                // 文档描述
                .description("描述：YV管理系统接口文档")
                // 版本
                .version("1.0")
                // 联系人信息
                .contact(new Contact("陈小龙", "", ""))
                // 版权
                .license("")
                // 版权地址
                .licenseUrl("")
                .build();
    }
}
