package pers.cxl.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 个人配置
 */
@Data
@Component
@ConfigurationProperties(prefix = "yv")
public class YVConfig {

    private String yv;
    private String name;
    private String version;

}
