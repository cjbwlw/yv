package pers.cxl.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 对象存储配置
 */
@Data
@Component
@ConfigurationProperties(prefix = "oss")
public class OSSConfig {


    private String bucket;
    private String endpoint;
    private String accessKeyId;
    private String secretAccessKey;

}
