package pers.cxl.common.handler;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pers.cxl.common.exception.GlobalException;
import pers.cxl.common.result.R;
import pers.cxl.common.result.ResultEnum;

/**
 * 全局异常处理器
 */
@RestControllerAdvice //(basePackages = {"pers.cxl.service.system.controller"})
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {GlobalException.class})
    public R<Object> globalException(GlobalException e){
        R<Object> r = new R<>();
        r.build(ResultEnum.RESULT_FAIL).setMessage(e.getMessage());
        return r;
    }

}
