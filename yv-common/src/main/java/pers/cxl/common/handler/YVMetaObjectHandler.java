package pers.cxl.common.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pers.cxl.common.util.ThreadLocalUtil;

import java.time.LocalDateTime;

/**
 * 自动填充处理器
 */
@Slf4j
@Component
public class YVMetaObjectHandler implements MetaObjectHandler {

    @Autowired
    ThreadLocalUtil<Long> threadLocalUtil;

    @Override
    public void insertFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, "registerTime", LocalDateTime::now, LocalDateTime.class);
        this.strictInsertFill(metaObject, "createTime", LocalDateTime::now, LocalDateTime.class);
        this.strictInsertFill(metaObject, "createUser", () -> threadLocalUtil.get(), Long.class);
        this.strictUpdateFill(metaObject, "updateTime", LocalDateTime::now, LocalDateTime.class); // 起始版本 3.3.3(推荐)
        this.strictInsertFill(metaObject, "updateUser", () -> threadLocalUtil.get(), Long.class);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictUpdateFill(metaObject, "updateTime", LocalDateTime::now, LocalDateTime.class); // 起始版本 3.3.3(推荐)
        this.strictInsertFill(metaObject, "updateUser", () -> threadLocalUtil.get(), Long.class);
    }
}
