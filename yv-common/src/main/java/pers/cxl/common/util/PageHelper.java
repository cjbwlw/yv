package pers.cxl.common.util;

import org.springframework.stereotype.Component;
import pers.cxl.model.system.dto.QueryDTO;

/**
 * 分页工具
 */
@Component
public class PageHelper {

    public void startPage(QueryDTO queryDTO){
        // 起始数 = (当前页)*每页显示的条数  当前页从1开始
        if (queryDTO.getNum()==0){
            queryDTO.setNum(1);
            queryDTO.setStart(queryDTO.getSize());
        }
        else queryDTO.setStart((queryDTO.getNum()-1) * queryDTO.getSize());
    }

}
