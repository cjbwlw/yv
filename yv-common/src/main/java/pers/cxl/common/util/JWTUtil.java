package pers.cxl.common.util;

import io.jsonwebtoken.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import pers.cxl.model.system.entity.User;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * jwt登录凭证工具类
 */
@Slf4j
@Data
@Component
@PropertySource(value = {"classpath:bootstrap.yml"})
@ConfigurationProperties(prefix = "jwt")
public class JWTUtil {

    private Long expiration = 1000*60*60*24L;

    private String signature = "yv-cxl";

    public static void main(String[] args) {
        System.out.println(new JWTUtil().getToken(new User().setId(1L)));
        System.out.println(new JWTUtil().parserToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ" +
                "1c2VyIjoiWVYiLCJzdWIiOiJsb2dpbi1hZG1pbi1zeXN0ZW0iLCJleHAiOjE2OTk5NDQ0NTcsImp0aSI6IjEifQ.fBBLPjy-pfBTVDlggStB8CwfeE4c77bFigfpZe3C8J8"));

        HashSet<Object> set = new HashSet<>();
        set.add(1);
        set.add(55);
        set.add(485);
        set.add(48);
        List<Object> list = set.stream().sorted().collect(Collectors.toList());
        list.forEach(System.out::println);
    }

    public String getToken(User user){
        JwtBuilder jwtBuilder = Jwts.builder();
        return jwtBuilder
                // header
                .setHeaderParam("typ", "JWT")
                .setHeaderParam("alg", "HS256")
                // payload
                .claim("user", user.getName())
                .setSubject("login-admin-system")
                .setExpiration(new Date(System.currentTimeMillis() + expiration))
                .setId(user.getId() + "")
                // signature
                .signWith(SignatureAlgorithm.HS256, signature)
                .compact();
    }

    public boolean parserToken(String token){
        JwtParser jwtParser = Jwts.parser();
        try {
            Jws<Claims> claimsJws = jwtParser.setSigningKey(signature).parseClaimsJws(token);
//            log.info("JWT有效期：{}", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(claimsJws.getBody().getExpiration()));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public Long getId(String token){
        JwtParser jwtParser = Jwts.parser();
        Jws<Claims> claimsJws = jwtParser.setSigningKey(signature).parseClaimsJws(token);
        Long id = new Long(claimsJws.getBody().getId());
        return id;
    }

}
