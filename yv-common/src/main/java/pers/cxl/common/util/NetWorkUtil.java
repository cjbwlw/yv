package pers.cxl.common.util;

import org.springframework.stereotype.Component;
import pers.cxl.common.constant.MessageConstant;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 网络工具类
 */
@Component
public class NetWorkUtil {

    public String getIP(){
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            return MessageConstant.UNKNOWN_IP;
        }
    }

}
