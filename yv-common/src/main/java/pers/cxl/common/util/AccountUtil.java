package pers.cxl.common.util;

import org.apache.commons.lang3.RandomUtils;
import org.springframework.stereotype.Component;

@Component
public class AccountUtil {


    public static void main(String[] args) {
        System.out.println(System.getProperty("user.dir"));
        System.out.println(new AccountUtil().generateAccount());
    }

    /**
     * 生成账号
     * @return 返回账号
     */
    public String generateAccount(){
        return (System.currentTimeMillis()/ RandomUtils.nextLong(1000,9999))+"";
    }

}
