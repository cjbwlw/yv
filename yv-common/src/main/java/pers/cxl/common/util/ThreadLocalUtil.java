package pers.cxl.common.util;

import org.springframework.stereotype.Component;

/**
 * 线程工具
 * @param <T>
 */
@Component
public class ThreadLocalUtil<T> {

    private final ThreadLocal<T> threadLocal = new ThreadLocal<>();

    public void set(T data){
        threadLocal.set(data);
    }

    public T get(){
        return threadLocal.get();
    }

    public void remove(){
        threadLocal.remove();
    }

}
