package pers.cxl.common.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.concurrent.LinkedTransferQueue;

@Slf4j
@Component
public class PasswordUtil {


    public static void main(String[] args) {
        System.out.println(new PasswordUtil().MD5("123456"));

        LinkedTransferQueue<Object> queue = new LinkedTransferQueue<>();
        queue.put(1);
        queue.put(2);
        queue.put(3);
        Object poll = queue.poll();
        System.out.println(poll);
        queue.remove(2);
        System.out.println(queue);

    }

    /**
     * md5算法加密
     * @param pwd
     * @return
     */
    public String MD5(String pwd){
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update((pwd+"7367556").getBytes(StandardCharsets.UTF_8));
            byte[] result = md5.digest();
            return parseByte2HexStr(result);
        } catch (Exception e) {
            log.info("密码加密失败===>{}",e.getMessage());
            return null;
        }
    }

    private String parseByte2HexStr(byte[] buf){
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < buf.length; i++) {
            String hex = Integer.toHexString(buf[i] & 0xFF);
            if (hex.length() == 1){
                hex = '0' +hex;
            }
            sb.append(hex.toUpperCase());
        }
        return sb.toString();
    }

}
