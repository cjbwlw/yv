package pers.cxl.common.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 统一返回结果类(泛型)
 * @param <T>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class R<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer code;
    private String message;
    private T data;

    public R<T> build(ResultEnum re, T data){
        this.code = re.code;
        this.message = re.message;
        this.data = data;
        return this;
    }

    public R<T> build(ResultEnum re){
        this.code = re.code;
        this.message = re.message;
        return this;
    }

}
