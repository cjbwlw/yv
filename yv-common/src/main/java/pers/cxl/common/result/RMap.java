package pers.cxl.common.result;

import java.util.HashMap;

/**
 * 统一返回结果(Map)
 */
public class RMap extends HashMap<String,Object> {

    private static final String CODE = "code";
    private static final String MESSAGE = "message";
    private static final String DATA = "data";

    //使用单例模式
    private static final RMap r = new RMap();

    private RMap(){}

    public static RMap success(){
        r.remove("data");
        r.push(CODE, ResultEnum.RESULT_SUCCESS.code);
        r.push(MESSAGE, ResultEnum.RESULT_SUCCESS.message);
        return r;
    }

    public static RMap fail(){
        r.remove("data");
        r.push(CODE, ResultEnum.RESULT_FAIL.code);
        r.push(MESSAGE, ResultEnum.RESULT_FAIL.message);
        return r;
    }

    public static RMap build(ResultEnum resultEnum){
        r.remove("data");
        r.push(CODE,resultEnum.code);
        r.push(MESSAGE,resultEnum.message);
        return r;
    }

    public static RMap build(ResultEnum resultEnum, Object data){
        r.remove("data");
        r.push(CODE,resultEnum.code);
        r.push(MESSAGE,resultEnum.message);
        r.push(DATA,data);
        return r;
    }

    public RMap push(String k,Object v){
        super.put(k,v);
        return this;
    }

}
