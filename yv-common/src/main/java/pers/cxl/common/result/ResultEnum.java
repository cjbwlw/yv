package pers.cxl.common.result;

/**
 * 返回结果枚举
 */
public enum ResultEnum {

    RESULT_SUCCESS(1000,"成功"),
    RESULT_FAIL(5000,"失败")
    ;

    public final Integer code;
    public final String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
