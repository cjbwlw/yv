package pers.cxl.common.result;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 返回页面列表对象
 */
@Data
@Builder
public class ResultPage {

    /** 总数 */
    private long total;
    /** 列表 */
    private List list;

}
