# YV

#### 介绍

我的毕设项目

#### 项目结构

- sql: 数据库文件
- yv-admin-ui: 后台管理系统
- yv-common: 公共模块
- yv-model: dto(数据传输对象),entity(数据库实体),vo(视图对象)
- yv-service: 微服务（手机app,gateway网关,sso认证中心,系统）
- yv-user-app: 手机app

#### 演示
<table>
    <tr>
        <td>
            <img src="./img/演示/1.png">
        </td>
        <td>
            <img src="./img/演示/2.png">
        </td>
    </tr>
    <tr>
        <td>
            <img src="./img/演示/3.png">
        </td>
        <td>
            <img src="./img/演示/4.png">
        </td>
    </tr>
    <tr>
        <td>
            <img src="./img/演示/5.png">
        </td>
        <td>
            <img src="./img/演示/6.png">
        </td>
    </tr>
    <tr>
        <td>
            <img src="./img/演示/7.png">
        </td>
    </tr>
</table>