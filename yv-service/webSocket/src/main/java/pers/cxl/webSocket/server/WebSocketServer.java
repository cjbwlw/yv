package pers.cxl.webSocket.server;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pers.cxl.webSocket.domain.Message;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@ServerEndpoint("/webSocket/{userID}/{fid}")
@Component
public class WebSocketServer {

    private static final ConcurrentHashMap<String,Session> sessionPool = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, LinkedTransferQueue<Message>> messageQueue = new ConcurrentHashMap<>();

    @OnOpen
    public void onOpen(Session session, @PathParam(value = "userID") String userID,@PathParam(value = "fid") String fid){
        log.info("用户连接成功，userID:{},fid:{}",userID,fid);
        AtomicBoolean flag = new AtomicBoolean(true);
        // 判断连接池是否有当前连接用户，有不添加，无则添加
        sessionPool.forEach((u,s)->{
            if (userID.equals(u)){
                flag.set(false);
            }
        });
        if (flag.get()){
            sessionPool.put(userID,session);
        }

        LinkedTransferQueue<Message> queue = messageQueue.get(userID);
        if (queue!=null){
            // 如果消息队列有消息，则遍历发送
            queue.forEach(m->{
                try {
                    if (Objects.equals(m.getFrom(), fid)){
                        session.getBasicRemote().sendText(JSON.toJSONString(m));
                        queue.remove(m);
                    }
                } catch (IOException e) {
                    throw new RuntimeException("发送消息失败");
                }
            });
        }
    }

    @OnClose
    public void onClose(@PathParam(value = "userID") String userID){
        sessionPool.remove(userID);
    }

    @OnMessage
    public void onMessage(@PathParam(value = "userID") String userID, Session session,String message){
        log.info("收到消息：userID=》{},message=>{}",userID,message);
        Message msg = JSON.parseObject(message, Message.class);
//        log.info("收到消息：{},来自于：{},发送到：{}",message,msg.getFrom(),msg.getTo());
        String dateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        msg.setDate(dateTime);
        sendMessage(msg);
    }

    @OnError
    public void onError(Session session, Throwable throwable){
        System.out.println("发生错误");
        throwable.printStackTrace();
    }

    public void sendMessage(Message message){
        Session session = sessionPool.get(message.getTo());
        try {
            if (session!=null){
                session.getBasicRemote().sendText(JSON.toJSONString(message));
            } else{
                // 如果用户不在线存储在队列中
                if (messageQueue.get(message.getTo())==null){
                    LinkedTransferQueue<Message> newQueue = new LinkedTransferQueue<>();
                    newQueue.put(message);
                    messageQueue.put(message.getTo(),newQueue);
                }else {
                    LinkedTransferQueue<Message> queue = messageQueue.get(message.getTo());
                    queue.put(message);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("发送消息失败");
        }
    }

}
