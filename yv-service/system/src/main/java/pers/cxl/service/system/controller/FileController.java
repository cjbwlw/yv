package pers.cxl.service.system.controller;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.OSSObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pers.cxl.common.config.OSSConfig;
import pers.cxl.common.constant.MessageConstant;
import pers.cxl.common.exception.GlobalException;
import pers.cxl.common.result.R;
import pers.cxl.common.result.ResultEnum;
import pers.cxl.model.system.common.Date;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Api(tags = "文件管理")
@Slf4j
@Controller
@RequestMapping("admin/system/file")
public class FileController {

    @Autowired
    private OSSConfig ossConfig;

    /**
     * 获取本地资源路径
     * @return
     */
    public String getLocalURL(){
        String path = System.getProperty("user.dir");
        log.info("打包之后的图片路径5256:{}",path);
        return System.getProperty("user.dir")+"\\";
//            return new File(ResourceUtils.getURL("classpath:").getPath()).getParentFile().getParentFile()
//                    .getPath()+ "\\src\\main\\resources\\";
    }

    @ApiOperation("文件上传到本地")
    @PostMapping("upload")
    @ResponseBody
    public R<Object> upload(@RequestBody List<MultipartFile> fileList) {
        String basePath = getLocalURL();
        List<String> pathList = new ArrayList<>();
        if (ObjectUtils.isEmpty(fileList)) {
            throw new GlobalException(MessageConstant.NO_FILE);
        }
        fileList.forEach(f -> {
            try {
                String filePath = getFilePath(f.getOriginalFilename());
                pathList.add(URLEncoder.encode(filePath,"UTF-8"));
                filePath = basePath + filePath;
                File file = new File(filePath);
                if (!file.exists()){
                    file.mkdirs();
                }
                f.transferTo(file);
            } catch (Exception e) {
                log.error("文件上传失败===>{}", e.getMessage());
                throw new GlobalException(MessageConstant.UPLOAD_ERR);
            }
        });
        return new R<>().build(ResultEnum.RESULT_SUCCESS, pathList);
    }

    @ApiOperation("文件预览")
    @GetMapping("preview")
    public void preview(String path, HttpServletResponse response) {
        FileInputStream in = null;
        ServletOutputStream outputStream = null;
        try {
            //文件后缀
            String suffix = path.substring(path.lastIndexOf(".")+1);
            switch (suffix){
                case "mp4":
                    // 设置 Content-Type 响应头
                    response.setContentType("video/mp4");
                    // 设置 Content-Disposition 响应头，告诉浏览器将视频流嵌入到页面中
//                    response.setHeader("Content-Disposition", "inline");
                    break;
            }
            String url = getLocalURL() + path;
            File file = new File(url);
            in = new FileInputStream(file);
            outputStream = response.getOutputStream();
            IOUtils.copy(in, outputStream);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }


    @ApiOperation("文件上传到阿里云oss")
    @PostMapping("uploadOSS")
    @ResponseBody
    public R<Object> uploadOss(@RequestBody List<MultipartFile> fileList) {
        List<String> pathList = new ArrayList<>();
        if (ObjectUtils.isEmpty(fileList)) {
            throw new GlobalException(MessageConstant.NO_FILE);
        }
        OSS ossClient = new OSSClientBuilder().build(ossConfig.getEndpoint(), ossConfig.getAccessKeyId(), ossConfig.getSecretAccessKey());
        fileList.forEach(f -> {
            String filePath = getFilePath(f.getOriginalFilename());
            try {
                ossClient.putObject(ossConfig.getBucket(), filePath, f.getInputStream());
                filePath = "https://" + ossConfig.getBucket() + "." + ossConfig.getEndpoint() + "/" + filePath;
                pathList.add(filePath);
            } catch (Exception e) {
                log.error("文件上传失败===>{}", e.getMessage());
                throw new GlobalException(MessageConstant.UPLOAD_ERR);
            } finally {
                if (ossClient != null) {
                    ossClient.shutdown();
                }
            }
        });
        return new R<>().build(ResultEnum.RESULT_SUCCESS, pathList);
    }

    @ApiOperation("文件下载")
    @GetMapping("download")
    public R<Object> download(String fileName,
                              HttpServletResponse response) {
        // 设置响应头为下载
        response.setContentType("application/x-download");

        // 浏览器以附件形式下载
        response.addHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(), StandardCharsets.UTF_8));
        response.setCharacterEncoding("UTF-8");

        // ossObject包含文件所在的存储空间名称、文件名称、文件元信息以及一个输入流。
        OSS ossClient = new OSSClientBuilder().build(ossConfig.getEndpoint(), ossConfig.getAccessKeyId(), ossConfig.getSecretAccessKey());
        OSSObject ossObject = ossClient.getObject(ossConfig.getBucket(), fileName);
        try {
            // 读取文件内容。
            BufferedInputStream in = new BufferedInputStream(ossObject.getObjectContent());
            BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
            byte[] buffer = new byte[1024];
            int lenght;
            while ((lenght = in.read(buffer)) != -1) {
                out.write(buffer, 0, lenght);
            }
            out.flush();
            out.close();
            in.close();
            return new R<>().build(ResultEnum.RESULT_SUCCESS);
        } catch (Exception e) {
            throw new GlobalException(MessageConstant.DOWNLOAD_ERR);
        } finally {
            ossClient.shutdown();
        }
    }

    /**
     * 生成路径以及文件名
     */
    private static String getFilePath(String sourceFileName) {
        Date date = new Date();
        return "img\\" + date.toString("yyyy") + "\\" + date.toString("MM") + "\\"
                + date.toString("dd") + "\\" + System.currentTimeMillis()
                + RandomUtils.nextLong(1000, 99999) + "."
                + StringUtils.substringAfterLast(sourceFileName, ".");
    }

}
