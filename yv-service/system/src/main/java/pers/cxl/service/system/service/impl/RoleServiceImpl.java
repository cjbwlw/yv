package pers.cxl.service.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import pers.cxl.common.constant.MessageConstant;
import pers.cxl.common.result.ResultPage;
import pers.cxl.common.util.PageHelper;
import pers.cxl.model.system.dto.QueryRoleDTO;
import pers.cxl.model.system.dto.RoleDTO;
import pers.cxl.model.system.entity.Role;
import pers.cxl.model.system.entity.User;
import pers.cxl.service.system.mapper.RoleMapper;
import pers.cxl.service.system.mapper.UserMapper;
import pers.cxl.service.system.service.RoleService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper,Role> implements RoleService{

    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    PageHelper pageHelper;
    @Override
    public ResultPage list(QueryRoleDTO queryRoleDTO) {
        //分页条件查询菜单数据
        pageHelper.startPage(queryRoleDTO);
        List<Role> list = roleMapper.list(queryRoleDTO);
        List<RoleDTO> roleDTOS = list.stream().map(r -> {
            RoleDTO roleDTO = new RoleDTO();
            User createUser = userMapper.selectById(r.getCreateUser());
            User updateUser = userMapper.selectById(r.getUpdateUser());
            BeanUtils.copyProperties(r, roleDTO);
            roleDTO.setCreateUser(ObjectUtils.isEmpty(createUser)?MessageConstant.UNKNOWN_USER:createUser.getNickname());
            roleDTO.setUpdateUser(ObjectUtils.isEmpty(updateUser)?MessageConstant.UNKNOWN_USER:updateUser.getNickname());
            return roleDTO;
        }).collect(Collectors.toList());
        roleDTOS = roleDTOS.stream()
                .skip(queryRoleDTO.getStart())
                .limit(queryRoleDTO.getSize())
                .collect(Collectors.toList());
        return ResultPage.builder().list(roleDTOS).total(list.size()).build();
    }

    @Override
    public void change(RoleDTO roleDTO) {
        Role role = new Role();
        BeanUtils.copyProperties(roleDTO,role);
        baseMapper.updateById(role);
    }

    @Override
    public void remove(List<Long> ids) {
        baseMapper.deleteBatchIds(ids);
    }

    @Override
    public void addRole(RoleDTO roleDTO) {
        Role role = new Role();
        BeanUtils.copyProperties(roleDTO,role);
        baseMapper.insert(role);
    }

    @Override
    public List<RoleDTO> allList() {
        List<Role> list = baseMapper.selectList(null);
        return list.stream().map(r -> {
            RoleDTO roleDTO = new RoleDTO();
            BeanUtils.copyProperties(r, roleDTO);
            return roleDTO;
        }).collect(Collectors.toList());
    }
}
