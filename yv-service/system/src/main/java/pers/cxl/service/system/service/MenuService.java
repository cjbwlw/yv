package pers.cxl.service.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import pers.cxl.common.result.ResultPage;
import pers.cxl.model.system.dto.MenuDTO;
import pers.cxl.model.system.dto.QueryMenuDTO;
import pers.cxl.model.system.entity.Menu;

import java.util.List;

public interface MenuService extends IService<Menu> {

    /**
     * 获取菜单列表
     * @param queryMenuDTO 查询参数
     * @return 页面对象
     */
    ResultPage list(QueryMenuDTO queryMenuDTO);

    /**
     *      * 修改菜单状态
     * @param menuDTO
     */
    void status(MenuDTO menuDTO);

    /**
     * 添加菜单
     * @param menuDTO
     */
    void addMenu(MenuDTO menuDTO);

    /**
     * 修改菜单
     * @param menuDTO
     */
    void change(MenuDTO menuDTO);

    /**
     * 批量，单独删除菜单
     * @param ids
     */
    void removeMenu(List<Long> ids);

    /**
     * 查询所有菜单列表
     * @return
     */
    List<Menu> allList();

}
