package pers.cxl.service.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import pers.cxl.common.result.ResultPage;
import pers.cxl.model.system.dto.*;
import pers.cxl.model.system.entity.User;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface UserService extends IService<User> {

    /**
     * 用户注册
     * @param registerUserDTO
     * @return
     */
    UserDTO registry(RegisterUserDTO registerUserDTO);

    /**
     * 查询用户列表
     * @param queryUserDTO
     * @return
     */
    ResultPage list(QueryUserDTO queryUserDTO);

    /**
     * 添加角色
     * @param userDTO
     */
    void addUser(UserDTO userDTO);

    /**
     * 删除角色
     * @param ids
     */
    void remove(List<Long> ids);

    /**
     * 修改用户状态
     * @param userDTO
     */
    void status(UserDTO userDTO);

    /**
     * 修改用户信息
     * @param userDTO
     */
    void change(UserDTO userDTO);

    /**
     * 用户登录
     * @param loginUserDTO
     * @return
     */
    UserDTO login(LoginUserDTO loginUserDTO);

    /**
     * 修改用户密码
     * @param changeUserPwdDTO
     */
    void changePwd(ChangeUserPwdDTO changeUserPwdDTO);

    /**
     * 根据账号查询用户
     * @param name
     * @return
     */
    UserDTO searchByAccount(String name);

    Map<String, Collection> visualData();

}
