package pers.cxl.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.mockito.internal.util.collections.ListUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import pers.cxl.common.constant.MessageConstant;
import pers.cxl.common.exception.GlobalException;
import pers.cxl.common.result.ResultPage;
import pers.cxl.common.util.*;
import pers.cxl.model.system.dto.*;
import pers.cxl.model.system.entity.Role;
import pers.cxl.model.system.entity.User;
import pers.cxl.service.system.mapper.RoleMapper;
import pers.cxl.service.system.mapper.UserMapper;
import pers.cxl.service.system.service.UserService;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Transactional
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private PageHelper pageHelper;
    @Autowired
    private NetWorkUtil netWorkUtil;
    @Autowired
    private JWTUtil jwtUtil;
    @Autowired
    private ThreadLocalUtil<Long> threadLocalUtil;

    @Autowired
    private AccountUtil accountUtil;
    @Autowired
    private PasswordUtil passwordUtil;

    @Override
    public UserDTO registry(RegisterUserDTO registerUserDTO) {
        User user = new User();
        BeanUtils.copyProperties(registerUserDTO,user);
        // 生成账号,登录ip,登录时间,加密
        user.setLoginTime(LocalDateTime.now())
                .setIp(netWorkUtil.getIP())
                .setName(accountUtil.generateAccount())
                .setPassword(passwordUtil.MD5(user.getPassword()));
        //注册
        baseMapper.insert(user);
        UserDTO userDTO = new UserDTO();
        User newUser = baseMapper.selectById(user.getId());
        BeanUtils.copyProperties(newUser,userDTO);
        userDTO.setToken(jwtUtil.getToken(user));
        return userDTO;
    }
    @Override
    public ResultPage list(QueryUserDTO queryUserDTO) {
        //分页条件查询菜单数据
        pageHelper.startPage(queryUserDTO);
        List<User> list = userMapper.list(queryUserDTO);
        List<UserDTO> userDTOS = list.stream().map(u -> {
            UserDTO userDTO = new UserDTO();
            Role role = roleMapper.selectById(u.getRid());
            userDTO.setRoleName(role.getName());
            BeanUtils.copyProperties(u, userDTO);
            return userDTO;
        }).collect(Collectors.toList());
        userDTOS = userDTOS.stream()
                .skip(queryUserDTO.getStart())
                .limit(queryUserDTO.getSize())
                .collect(Collectors.toList());
        return ResultPage.builder().list(userDTOS).total(list.size()).build();
    }

    @Override
    public void addUser(UserDTO userDTO) {
        User user = new User();
        BeanUtils.copyProperties(userDTO,user);
        baseMapper.insert(user);
    }

    @Override
    public void remove(List<Long> ids) {
        baseMapper.deleteBatchIds(ids);
    }

    @Override
    public void status(UserDTO userDTO) {
        User user = new User();
        BeanUtils.copyProperties(userDTO,user);
        LambdaUpdateWrapper<User> wrapper = new LambdaUpdateWrapper<User>()
                .set(User::getStatus, user.getStatus())
                .eq(User::getId, user.getId());
        baseMapper.update(user,wrapper);
    }

    @Override
    public void change(UserDTO userDTO) {
        // 修改角色
        User user = new User();
        BeanUtils.copyProperties(userDTO,user);
        baseMapper.updateById(user);
    }

    @Override
    public UserDTO login(LoginUserDTO loginUserDTO) {
        User user = baseMapper.selectOne(new LambdaQueryWrapper<User>()
                .eq(User::getName, loginUserDTO.getUsername())
                .eq(User::getPassword, passwordUtil.MD5(loginUserDTO.getPassword())));
        if (ObjectUtils.isEmpty(user)){
            throw new GlobalException(MessageConstant.LOGIN_ERR);
        }else if (user.getStatus()==0){
            throw new GlobalException(MessageConstant.ACCOUNT_FORBIDDEN);
        }
        // 更新登录时间和登录IP
        user.setLoginTime(LocalDateTime.now());
        user.setIp(netWorkUtil.getIP());
        baseMapper.updateById(user);
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(user,userDTO);
        // 生成并设置token
        String token = jwtUtil.getToken(user);
        userDTO.setToken(token);
        threadLocalUtil.set(user.getId());
        // 设置角色名称
        Role role = roleMapper.selectById(user.getRid());
        userDTO.setRoleName(role.getName());

        return userDTO;
    }

    @Override
    public void changePwd(ChangeUserPwdDTO changeUserPwdDTO) {
        String md5LowPwd = passwordUtil.MD5(changeUserPwdDTO.getOldPwd());
        User user = baseMapper.selectOne(new LambdaQueryWrapper<User>()
                .eq(User::getId, changeUserPwdDTO.getId()));
        if (user.getPassword().equals(md5LowPwd)){
            String newPwd = passwordUtil.MD5(changeUserPwdDTO.getNewPwd());
            user.setPassword(newPwd);
            baseMapper.updateById(user);
        }else {
            throw new GlobalException(MessageConstant.PASSWORD_ERR);
        }
    }

    @Override
    public UserDTO searchByAccount(String name) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<User>()
                .eq(User::getName, name);
        User user = baseMapper.selectOne(wrapper);
        UserDTO userDTO = new UserDTO();
        if (user != null){
            BeanUtils.copyProperties(user,userDTO);
            return userDTO;
        }
        return null;
    }

    @Override
    public Map<String, Collection> visualData() {
        Map<String, Collection> map = new HashMap<>();
        // 查询用户注册的使用年份集合
        List<LocalDateTime> yearList = userMapper.selectRegisterYear();
        HashSet<Integer> yearSet = new HashSet<>();
        yearList.forEach(y->{
            if(y!=null){
                int year = y.getYear();
                yearSet.add(year);
            }
        });
        // 排序年份
        List<Integer> year = yearSet.stream().sorted().collect(Collectors.toList());
        map.put("year",year);
        // 每年注册用户数量集合
//        HashMap<Integer, Integer> registerNum = new HashMap<>();
        ArrayList<Integer> registerNum = new ArrayList<>();
        year.forEach(y->{
            AtomicInteger j = new AtomicInteger();
            yearList.forEach(i->{
                if (i != null){
                    int y1 = i.getYear();
                    if (y1 == y){
                        j.addAndGet(1);
                    }
                }
            });
            registerNum.add(j.get());
        });
        map.put("regNum",registerNum);
        Integer i = registerNum.stream().max(Comparator.comparingInt(Integer::intValue)).get();
        ArrayList<Object> max = new ArrayList<>();
        registerNum.forEach(rn->{
            max.add(i);
        });
        map.put("max",max);
        Long count = baseMapper.selectCount(null);
        ArrayList<Object> countList = new ArrayList<>();
        countList.add(count);
        map.put("userCount",countList);
        return map;
    }

}
