package pers.cxl.service.system.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pers.cxl.common.result.R;
import pers.cxl.common.result.ResultEnum;
import pers.cxl.common.result.ResultPage;
import pers.cxl.model.system.dto.QueryRoleDTO;
import pers.cxl.model.system.dto.RoleDTO;
import pers.cxl.service.system.service.RoleService;

import java.util.List;

@Api(tags = "角色管理")
@Slf4j
@RestController
@RequestMapping("admin/system/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("allList")
    public R<List<RoleDTO>> allList(){
        log.info("查询所有角色信息");
        List<RoleDTO> list = roleService.allList();
        return new R<List<RoleDTO>>().build(ResultEnum.RESULT_SUCCESS,list);
    }

    @GetMapping("list")
    public R<ResultPage> list(QueryRoleDTO queryRoleDTO){
        log.info("查询角色信息===>{}",queryRoleDTO);
        ResultPage resultPage = roleService.list(queryRoleDTO);
        return new R<ResultPage>().build(ResultEnum.RESULT_SUCCESS,resultPage);
    }

    @PutMapping
    public R<Object> change(@RequestBody RoleDTO roleDTO){
        log.info("修改角色信息===》{}",roleDTO);
        roleService.change(roleDTO);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

    @PostMapping
    public R<Object> add(@RequestBody RoleDTO roleDTO){
        log.info("添加角色===》{}",roleDTO);
        roleService.addRole(roleDTO);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

    @DeleteMapping
    public R<Object> remove(@RequestBody List<Long> ids){
        log.info("删除角色===》{}",ids);
        roleService.remove(ids);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

}
