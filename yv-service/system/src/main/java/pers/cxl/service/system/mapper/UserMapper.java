package pers.cxl.service.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import pers.cxl.model.system.dto.QueryUserDTO;
import pers.cxl.model.system.entity.User;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {

    List<User> list(QueryUserDTO queryUserDTO);

    @Select("select register_time from user")
    List<LocalDateTime> selectRegisterYear();

}
