package pers.cxl.service.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import pers.cxl.model.system.dto.QueryRoleDTO;
import pers.cxl.model.system.entity.Role;

import java.util.List;

@Mapper
public interface RoleMapper extends BaseMapper<Role> {

    List<Role> list(QueryRoleDTO queryRoleDTO);
}
