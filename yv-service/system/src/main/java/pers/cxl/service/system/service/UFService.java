package pers.cxl.service.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import pers.cxl.model.system.dto.UserDTO;
import pers.cxl.model.system.entity.UF;

import java.util.List;

public interface UFService extends IService<UF> {
    List<Long> friendList(Long id);

    List<UserDTO> friendListEntity(Long id);

    boolean isFriend(Long myId,Long fid);

    void addFriend(UF uf);
}
