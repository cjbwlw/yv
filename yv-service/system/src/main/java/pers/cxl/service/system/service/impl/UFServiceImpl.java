package pers.cxl.service.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.cxl.model.system.dto.UserDTO;
import pers.cxl.model.system.entity.UF;
import pers.cxl.service.system.mapper.UFMapper;
import pers.cxl.service.system.service.UFService;

import java.util.List;

@Service
public class UFServiceImpl extends ServiceImpl<UFMapper, UF> implements UFService {

    @Autowired
    private UFMapper ufMapper;

    @Override
    public List<Long> friendList(Long id) {
        List<Long> data = ufMapper.getByUId(id);
        return data;
    }

    @Override
    public List<UserDTO> friendListEntity(Long id) {
        List<UserDTO> data = ufMapper.getByUIdOnEntity(id);
        return data;
    }

    @Override
    public boolean isFriend(Long myId,Long fid) {
        return ufMapper.isFriend(myId, fid) != null;
    }

    @Override
    public void addFriend(UF uf) {
        baseMapper.insert(uf);
        UF uf1 = new UF();
        uf1.setFid(uf.getUid()).setUid(uf.getFid());
        baseMapper.insert(uf1);
    }
}
