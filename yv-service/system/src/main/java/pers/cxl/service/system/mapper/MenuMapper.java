package pers.cxl.service.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import pers.cxl.model.system.dto.QueryMenuDTO;
import pers.cxl.model.system.entity.Menu;

import java.util.List;

@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

    List<Menu> list(QueryMenuDTO queryMenuDTO);

}
