package pers.cxl.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import pers.cxl.common.constant.MessageConstant;
import pers.cxl.common.result.ResultPage;
import pers.cxl.model.system.dto.MenuDTO;
import pers.cxl.model.system.dto.QueryMenuDTO;
import pers.cxl.model.system.entity.Menu;
import pers.cxl.model.system.entity.User;
import pers.cxl.service.system.mapper.MenuMapper;
import pers.cxl.service.system.mapper.UserMapper;
import pers.cxl.service.system.service.MenuService;
import pers.cxl.common.util.PageHelper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Transactional
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {

    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    PageHelper pageHelper;

    @Override
    public ResultPage list(QueryMenuDTO queryMenuDTO) {
        //分页条件查询菜单数据
        pageHelper.startPage(queryMenuDTO);
        //条件查询
        List<Menu> menus = menuMapper.list(queryMenuDTO);
        //递归构建菜单
        List<MenuDTO> root = recursionMenu(menus);
        //递归之后的长度
        long count = countMenu(queryMenuDTO);
        List<MenuDTO> root2;
        if (queryMenuDTO.getStart() >= 0 && queryMenuDTO.getSize() > 0) {
            root2 = root.stream().skip(queryMenuDTO.getStart())
                    .limit(queryMenuDTO.getSize())
                    .collect(Collectors.toList());
            return ResultPage.builder()
                    .total(count)
                    .list(root2)
                    .build();
        }
        return ResultPage.builder()
                .total(count)
                .list(root)
                .build();
    }

    @Override
    public void status(MenuDTO menuDTO) {
        Menu menu = new Menu();
        BeanUtils.copyProperties(menuDTO,menu);
        LambdaUpdateWrapper<Menu> wrapper = new LambdaUpdateWrapper<Menu>()
                .set(Menu::getStatus, menu.getStatus())
                .eq(Menu::getId, menu.getId());
        baseMapper.update(menu,wrapper);
    }

    @Override
    public void addMenu(MenuDTO menuDTO) {
        if (ObjectUtils.isEmpty(menuDTO.getPid())) {
            menuDTO.setPid(0L);
        }
        Menu menu = new Menu();
        BeanUtils.copyProperties(menuDTO,menu);
        baseMapper.insert(menu);
    }

    @Override
    public void change(MenuDTO menuDTO) {
        Menu menu = new Menu();
        BeanUtils.copyProperties(menuDTO,menu);
        baseMapper.updateById(menu);
    }

    @Override
    public void removeMenu(List<Long> ids) {
        baseMapper.deleteBatchIds(ids);
    }

    @Override
    public List<Menu> allList() {
        return baseMapper.selectList(null);
    }

    /**
     * 递归菜单之后从菜单长度
     * @return
     */
    public long countMenu(QueryMenuDTO queryMenuDTO) {
        LambdaQueryWrapper<Menu> wrapper = new LambdaQueryWrapper<Menu>()
                .like(!ObjectUtils.isEmpty(queryMenuDTO.getTitle()), Menu::getTitle, queryMenuDTO.getTitle())
                .eq(!ObjectUtils.isEmpty(queryMenuDTO.getStatus()), Menu::getStatus, queryMenuDTO.getStatus());
        List<Menu> list = baseMapper.selectList(wrapper);
        List<MenuDTO> menuDTOS = recursionMenu(list);
        return menuDTOS.size();
    }

    /**
     * 转换为DTO并设置创建用户和更新用户
     * @param menus
     * @return
     */
    public List<MenuDTO> toMenuListDTO(List<Menu> menus) {
        return menus.stream().map(menu -> {
            MenuDTO menuDTO = new MenuDTO();
            User updateUser = userMapper.selectById(menu.getUpdateUser());
            User createUser = userMapper.selectById(menu.getCreateUser());
            BeanUtils.copyProperties(menu, menuDTO);
            menuDTO.setCreateUser(ObjectUtils.isEmpty(createUser)?MessageConstant.UNKNOWN_USER:createUser.getNickname());
            menuDTO.setUpdateUser(ObjectUtils.isEmpty(updateUser)?MessageConstant.UNKNOWN_USER:updateUser.getNickname());
            return menuDTO;
        }).collect(Collectors.toList());
    }

    /**
     * 递归菜单
     * @param menus
     * @return
     */
    public List<MenuDTO> recursionMenu(List<Menu> menus){
        //转换为DTO并设置创建用户和更新用户
        List<MenuDTO> menuDTOS = toMenuListDTO(menus);
        //递归构建根菜单
        List<MenuDTO> root = menuDTOS.stream().filter(menuDTO -> Objects.equals(menuDTO.getPid(), 0L))
                .peek(menuDTO -> {
                    menuDTO.setSubmenu(buildSubmenu(menuDTO, menuDTOS));
                })
                .collect(Collectors.toList());
        // 递归构建非根菜单
        menuDTOS.stream().filter(menuDTO -> menuDTO.getPid() != 0L && notContainsSubmenu(menuDTO, root))
                .forEach(menuDTO -> {
                    root.add(menuDTO);
                    buildSubmenu(menuDTO,menuDTOS);
                });
        return root;
    }

    /**
     * 递归构建子菜单
     * @param root 父菜单
     * @param menuDTOS 菜单列表
     */
    public List<MenuDTO> buildSubmenu(MenuDTO root,List<MenuDTO> menuDTOS){
        return menuDTOS.stream().filter(menuDTO ->
                Objects.equals(menuDTO.getPid(), root.getId()))
                .peek(menuDTO ->
                        menuDTO.setSubmenu(buildSubmenu(menuDTO, menuDTOS))
                ).collect(Collectors.toList());
    }

    /**
     * root是否包含menu（包括子菜单）
     * @param menu 是否包含的菜单
     * @param root 菜单集合
     * @return true:不包含，false:包含
     */
    public boolean notContainsSubmenu(MenuDTO menu,List<MenuDTO> root) {
        if (root.contains(menu)){
            return false;
        }else {
            return root.stream().filter(menuDTO ->
                    !ObjectUtils.isEmpty(menuDTO.getSubmenu()))
                    .allMatch(menuDTO ->
                            notContainsSubmenu(menu, menuDTO.getSubmenu()));
        }
    }

}
