package pers.cxl.service.system.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pers.cxl.common.result.R;
import pers.cxl.common.result.ResultEnum;
import pers.cxl.common.result.ResultPage;
import pers.cxl.model.app.entity.ApplyFriend;
import pers.cxl.model.system.dto.*;
import pers.cxl.model.system.entity.Role;
import pers.cxl.model.system.entity.UF;
import pers.cxl.model.system.entity.User;
import pers.cxl.service.system.service.RoleService;
import pers.cxl.service.system.service.UFService;
import pers.cxl.service.system.service.UserService;

import javax.validation.constraints.Null;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Api(tags = "用户管理")
@Slf4j
@RestController
@RequestMapping("admin/system/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private UFService ufService;

    @ApiOperation("可视化数据")
    @GetMapping("visualData")
    public R<Object> visualData(){
        Map<String, Collection> data = userService.visualData();
        return new R<>().build(ResultEnum.RESULT_SUCCESS,data);
    }

    @ApiOperation("添加用户")
    @PutMapping("addFriend")
    public R<Object> addFriend(@RequestBody UF uf){
        ufService.addFriend(uf);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

    @ApiOperation("根据账号查询用户")
    @PostMapping("search/account/{name}")
    public R<UserDTO> searchByAccount(@PathVariable String name){
        UserDTO data = userService.searchByAccount(name);
        return new R<UserDTO>().build(ResultEnum.RESULT_SUCCESS,data);
    }

    @ApiOperation("查询用户好友列表")
    @PostMapping("friend/entity/{id}")
    public R<List<UserDTO>> friendListEntity(@PathVariable Long id){
        List<UserDTO> data = ufService.friendListEntity(id);
        return new R<List<UserDTO>>().build(ResultEnum.RESULT_SUCCESS,data);
    }

    @ApiOperation("查询用户好友id")
    @PostMapping("friend/list/{id}")
    public R<List<Long>> friendList(@PathVariable Long id){
        List<Long> data = ufService.friendList(id);
        return new R<List<Long>>().build(ResultEnum.RESULT_SUCCESS,data);
    }

    @ApiOperation("修改密码")
    @PutMapping("changePwd")
    public R<Object> changePwd(@RequestBody ChangeUserPwdDTO changeUserPwdDTO){
        log.info("修改用户密码===>{}",changeUserPwdDTO);
        userService.changePwd(changeUserPwdDTO);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

    @ApiOperation("用户注册")
    @PostMapping("registry")
    public R<UserDTO> registry(@RequestBody RegisterUserDTO registerUserDTO){
        log.info("注册用户===>{}",registerUserDTO);
        UserDTO userDTO = userService.registry(registerUserDTO);
        return new R<UserDTO>().build(ResultEnum.RESULT_SUCCESS,userDTO);
    }

    /**
     *
     * @param id
     * @param myID 当前登录用户id
     * @return
     */
    @ApiOperation(value = "根据id获取用户信息")
    @GetMapping("{id}")
    public R<UserDTO> getByID(@PathVariable Long id,@RequestParam("myID") Long myID){
        log.info("根据id获取用户信息===》{}",id);
        User user = userService.getById(id);
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(user,userDTO);
        Role role = roleService.getById(user.getRid());
        userDTO.setRoleName(role.getName());
        if (Objects.equals(id, myID))
            userDTO.setIsFriend(true);
        else
            userDTO.setIsFriend(ufService.isFriend(myID,id));
        return new R<UserDTO>().build(ResultEnum.RESULT_SUCCESS,userDTO);
    }

    @ApiOperation(value = "用户登录")
    @PostMapping("login")
    public R<UserDTO> login(@RequestBody LoginUserDTO loginUserDTO){
        log.info("登录用户===》{}",loginUserDTO);
        UserDTO userDTO = userService.login(loginUserDTO);
        return new R<UserDTO>().build(ResultEnum.RESULT_SUCCESS,userDTO);
    }

    @GetMapping("list")
    public R<ResultPage> list(QueryUserDTO queryUserDTO){
        log.info("查询用户列表 ===> {}",queryUserDTO);
        ResultPage resultPage = userService.list(queryUserDTO);
        return new R<ResultPage>().build(ResultEnum.RESULT_SUCCESS,resultPage);
    }

    @PostMapping
    public R<Object> add(@RequestBody UserDTO userDTO){
        log.info("查询用户列表 ===> {}",userDTO);
        userService.addUser(userDTO);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

    @DeleteMapping
    public R<Object> remove(@RequestBody List<Long> ids){
        log.info("删除角色===》{}",ids);
        userService.remove(ids);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

    @PutMapping
    public R<Object> change(@RequestBody UserDTO userDTO){
        log.info("修改用户信息===》{}",userDTO);
        userService.change(userDTO);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

    @ApiOperation(value = "修改用户状态")
    @PutMapping("status/{status}")
    public R<Object> status(@PathVariable Integer status,
                            @RequestBody UserDTO userDTO){
        Long id = userDTO.getId();
        log.info("修改用户状态 ===> id：{},状态：{}",id,status);
        userDTO.setStatus(status);
        userService.status(userDTO);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

}