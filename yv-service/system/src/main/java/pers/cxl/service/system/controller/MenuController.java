package pers.cxl.service.system.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;
import pers.cxl.common.result.R;
import pers.cxl.common.result.ResultEnum;
import pers.cxl.common.result.ResultPage;
import pers.cxl.model.system.dto.MenuDTO;
import pers.cxl.model.system.dto.QueryMenuDTO;
import pers.cxl.model.system.entity.Menu;
import pers.cxl.service.system.service.MenuService;

import java.util.List;

@Api(tags = "菜单管理")
@Slf4j
@RefreshScope // 自动刷新
@RestController
@RequestMapping("admin/system/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @ApiOperation(value = "查询所有菜单列表")
    @GetMapping("allList")
    public R<List<Menu>> allList(){
        log.info("查询所有菜单列表");
        List<Menu> list =  menuService.allList();
        return new R<List<Menu>>()
                .build(ResultEnum.RESULT_SUCCESS,list);
    }

    @ApiOperation(value = "查询菜单列表")
    @GetMapping("list")
    public R<ResultPage> list(QueryMenuDTO queryMenuDTO){
        log.info("查询菜单列表 ===> {}",queryMenuDTO);
        ResultPage page =  menuService.list(queryMenuDTO);
        return new R<ResultPage>()
                .build(ResultEnum.RESULT_SUCCESS,page);
    }

    @ApiOperation(value = "修改菜单状态")
    @PutMapping("status/{status}")
    public R<Object> status(@PathVariable Integer status,
                            @RequestBody MenuDTO menuDTO){
        Long id = menuDTO.getId();
        log.info("修改菜单状态 ===> id：{},状态：{}",id,status);
        menuDTO.setStatus(status);
        menuService.status(menuDTO);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

    @ApiOperation(value = "添加菜单")
    @PostMapping
    public R<Object> add(@RequestBody MenuDTO menuDTO){
        log.info("添加菜单 ===> {}",menuDTO);
        menuService.addMenu(menuDTO);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

    @ApiOperation(value = "修改菜单")
    @PutMapping
    public R<Object> change(@RequestBody MenuDTO menuDTO){
        log.info("修改菜单 ===> {}",menuDTO);
        menuService.change(menuDTO);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

    @ApiOperation(value = "删除菜单")
    @DeleteMapping
    public R<Object> remove(@RequestBody List<Long> ids){
        log.info("删除菜单 ===> {}",ids);
        menuService.removeMenu(ids);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

}
