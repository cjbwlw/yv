package pers.cxl.service.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import pers.cxl.model.system.dto.UserDTO;
import pers.cxl.model.system.entity.UF;

import java.util.List;

@Mapper
public interface UFMapper extends BaseMapper<UF> {

    @Select("select f_id from u_f where u_id = #{id}")
    List<Long> getByUId(@Param("id") Long id);

    @Select("SELECT f.id,f.rid,f.name,f.avatar,f.nickname,f.ip,f.login_time as loginTime,f.gender,f.id_no as idNo,f.phone,f.email,f.`status`,f.deleted,f.register_time as registerTime " +
            "from" +
            " user u join u_f uf " +
            "on u.id=uf.u_id JOIN user f " +
            "on uf.f_id=f.id " +
            "WHERE u.id = #{id}")
    List<UserDTO> getByUIdOnEntity(Long id);

    @Select("select u_id,f_id from u_f where u_id = #{myID} and f_id = #{fid}")
    UF isFriend(@Param("myID") Long myId,@Param(value = "fid") Long fid);
}
