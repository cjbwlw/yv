package pers.cxl.service.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import pers.cxl.common.result.ResultPage;
import pers.cxl.model.system.dto.QueryRoleDTO;
import pers.cxl.model.system.dto.RoleDTO;
import pers.cxl.model.system.entity.Role;

import java.util.List;

public interface RoleService extends IService<Role> {
    /**
     * 查询角色列表
     * @param queryRoleDTO
     * @return
     */
    ResultPage list(QueryRoleDTO queryRoleDTO);

    /**
     * 修改菜单信息
     * @param roleDTO
     */
    void change(RoleDTO roleDTO);

    /**
     * 批量删除角色
     * @param ids
     */
    void remove(List<Long> ids);

    /**
     * 添加角色
     * @param roleDTO
     */
    void addRole(RoleDTO roleDTO);

    /**
     * 查询所有角色信息
     * @return
     */
    List<RoleDTO> allList();

}