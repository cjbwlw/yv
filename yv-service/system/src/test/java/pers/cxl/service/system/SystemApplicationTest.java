package pers.cxl.service.system;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSClientBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pers.cxl.model.system.dto.MenuDTO;
import pers.cxl.service.system.mapper.UserMapper;
import pers.cxl.service.system.service.impl.MenuServiceImpl;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;

@SpringBootTest
public class SystemApplicationTest {

    @Autowired
    MenuServiceImpl menuService;

    @Autowired
    UserMapper userMapper;

    public static void main(String[] args) {
        OSS oss = new OSSClientBuilder().build("oss-cn-guangzhou.aliyuncs.com", "LTAI5tQtjYeWuUi9NWKAVCPu", "JHuRUHK0mc3ITWssIJFRCLuwoWYwAM");
        oss.putObject("yv-test","test/test.txt",new File("D:\\壁纸\\3f30472a880511ebb6edd017c2d2eca2.jpg"));
    }

    @Test
    public void testURLEncoding() throws UnsupportedEncodingException {
        System.out.println(URLEncoder.encode("img\\2023\\10\\31\\16987371244209476.jpg", "UTF-8"));
    }


    @Test
    public void testNotContainsSubmenuFunction(){
        MenuDTO o1 = new MenuDTO();
        MenuDTO o2 = new MenuDTO();
        o1.setSubmenu(new ArrayList<>());
        o2.setSubmenu(new ArrayList<>());
        MenuDTO o3 = new MenuDTO();
        o1.getSubmenu().add(o3);

        ArrayList<MenuDTO> list = new ArrayList<>();
        list.add(o1);
        list.add(o2);

        MenuDTO o4 = new MenuDTO();
        o4.setName("test");
        MenuDTO o5 = new MenuDTO();
        MenuDTO o6 = new MenuDTO();
        o6.setName("test1");
        o2.setSubmenu(new ArrayList<>());
        o2.getSubmenu().add(o5);
        o2.getSubmenu().add(o6);
        System.out.println(menuService.notContainsSubmenu(o4, list));
    }

}
