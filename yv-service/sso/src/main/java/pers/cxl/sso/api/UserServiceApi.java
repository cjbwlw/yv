package pers.cxl.sso.api;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import pers.cxl.common.result.R;
import pers.cxl.model.system.dto.LoginUserDTO;
import pers.cxl.model.system.dto.UserDTO;

@FeignClient(value = "system")
public interface UserServiceApi {

    @PostMapping("/admin/system/user/login")
    R<UserDTO> login(@RequestBody LoginUserDTO loginUserDTO);

}
