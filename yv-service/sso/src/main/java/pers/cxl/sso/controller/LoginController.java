package pers.cxl.sso.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.cxl.common.result.R;
import pers.cxl.common.result.ResultEnum;
import pers.cxl.common.util.JWTUtil;
import pers.cxl.model.system.dto.LoginUserDTO;
import pers.cxl.model.system.dto.UserDTO;
import pers.cxl.sso.api.UserServiceApi;

@Api("认证中心")
@Slf4j
@RestController
@RequestMapping("admin/sso")
public class LoginController {

    @Autowired
    private UserServiceApi userServiceApi;
//    @Autowired
//    private JWTUtil jwtUtil;

    @ApiOperation("远程调用用户登录")
    @PostMapping("login")
    public R<UserDTO> login(@RequestBody LoginUserDTO loginUserDTO){
        log.info("认证中心登录用户===》{}",loginUserDTO);
        // 远程调用用户登录接口
        R<UserDTO> res = userServiceApi.login(loginUserDTO);
        // 判断返回数据是否正常
        if (res.getCode()>=5000){
            return res;
        }
        return new R<UserDTO>().build(ResultEnum.RESULT_SUCCESS,res.getData());
    }

//    @PostMapping("authToken")
//    public R<Boolean> authToken(@RequestBody String token){
//        boolean flag = jwtUtil.parserToken(token);
//        return new R<Boolean>().build(ResultEnum.RESULT_SUCCESS, flag);
//    }

}
