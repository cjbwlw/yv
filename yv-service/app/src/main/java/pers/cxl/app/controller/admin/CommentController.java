package pers.cxl.app.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pers.cxl.app.service.CommentService;
import pers.cxl.common.result.R;
import pers.cxl.common.result.ResultEnum;
import pers.cxl.common.result.ResultPage;
import pers.cxl.model.app.dto.CommentDTO;

@RestController("adminCommentController")
@RequestMapping("admin/app/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @PostMapping("list")
    public R<ResultPage> list(@RequestBody CommentDTO commentDTO){
        ResultPage resultPage = commentService.pageList(commentDTO);
        return new R<ResultPage>().build(ResultEnum.RESULT_SUCCESS,resultPage);
    }

    @DeleteMapping("{id}")
    public R<Object> delByID(@PathVariable Long id){
        commentService.removeById(id);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

}
