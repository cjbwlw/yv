package pers.cxl.app.api;


import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import pers.cxl.app.config.FeignConfig;
import pers.cxl.common.result.R;
import pers.cxl.model.system.dto.RegisterUserDTO;
import pers.cxl.model.system.dto.UserDTO;
import pers.cxl.model.system.entity.UF;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.List;

@FeignClient(value = "system")
public interface UserServiceApi {

    @PutMapping("/admin/system/user/addFriend")
    R<Object> addFriend(@RequestBody UF uf);

    @PostMapping("/admin/system/user/registry")
    R<UserDTO> register(@RequestBody RegisterUserDTO registerUserDTO);

    @PostMapping("/admin/system/user/friend/list/{id}")
    R<List<Long>> friendList(@PathVariable(value = "id") Long id);

    @GetMapping("/admin/system/user/{id}")
    R<UserDTO> getByID(@PathVariable(value = "id") Long id,
                       @RequestParam("myID") Long myID);

    @PostMapping("/admin/system/user/friend/entity/{id}")
    R<List<UserDTO>> friendListEntity(@PathVariable(value = "id") Long id);

    @PostMapping("/admin/system/user/search/account/{name}")
    R<UserDTO> searchByAccount(@PathVariable(value = "name") String name);

}
