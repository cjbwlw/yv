package pers.cxl.app.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pers.cxl.app.api.UserServiceApi;
import pers.cxl.common.constant.MessageConstant;
import pers.cxl.common.exception.GlobalException;
import pers.cxl.common.result.R;
import pers.cxl.common.result.ResultEnum;
import pers.cxl.common.util.JWTUtil;
import pers.cxl.model.system.dto.RegisterUserDTO;
import pers.cxl.model.system.dto.UserDTO;

import javax.servlet.http.HttpServletRequest;

@Api(tags = "用户app")
@Slf4j
@RestController
@RequestMapping("user/app/user")
public class UserController {

    @Autowired
    private UserServiceApi userServiceApi;
    @Autowired
    private JWTUtil jwtUtil;

    @ApiOperation("注册用户")
    @PostMapping("registry")
    public R<UserDTO> registry(@RequestBody RegisterUserDTO registerUserDTO){
        log.info("注册用户===>{}",registerUserDTO);
        R<UserDTO> register = userServiceApi.register(registerUserDTO);
        if (register.getCode()>=5000){
            throw new GlobalException(MessageConstant.REGISTER_ERR);
        }
        return new R<UserDTO>().build(ResultEnum.RESULT_SUCCESS,register.getData());
    }

    @ApiOperation("根据id获取用户")
    @GetMapping("{id}")
    public R<UserDTO> getById(@PathVariable Long id, HttpServletRequest request){
        R<UserDTO> result = userServiceApi.getByID(id,jwtUtil.getId(request.getHeader("Token")));
        if (result.getCode()>=5000){
            throw new GlobalException(MessageConstant.RPC_ERR);
        }
        return new R<UserDTO>().build(ResultEnum.RESULT_SUCCESS,result.getData());
    }

}
