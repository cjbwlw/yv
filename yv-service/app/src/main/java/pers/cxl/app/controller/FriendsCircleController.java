package pers.cxl.app.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pers.cxl.app.service.FriendsCircleService;
import pers.cxl.common.result.R;
import pers.cxl.common.result.ResultEnum;
import pers.cxl.common.util.JWTUtil;
import pers.cxl.model.app.dto.CommentDTO;
import pers.cxl.model.app.dto.FriendsCircleDTO;
import pers.cxl.model.app.entity.FriendsCircleLike;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(tags = "朋友圈")
@RestController
@RequestMapping("user/app/friendsCircle")
public class FriendsCircleController {

    @Autowired
    private FriendsCircleService friendsCircleService;
    @Autowired
    private JWTUtil jwtUtil;

    @ApiOperation("根据用户id查询朋友圈列表")
    @PostMapping("list/{id}")
    public R<List<FriendsCircleDTO>> list(@PathVariable Long id,HttpServletRequest request){
        List<FriendsCircleDTO> data = friendsCircleService.findList(id,jwtUtil.getId(request.getHeader("Token")));
        return new R<List<FriendsCircleDTO>>().build(ResultEnum.RESULT_SUCCESS,data);
    }

    @ApiOperation("朋友圈点赞")
    @PutMapping("like/{flag}")
    public R<Object> like(@PathVariable Integer flag, @RequestBody FriendsCircleLike friendsCircleLike){
        friendsCircleService.like(flag,friendsCircleLike);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

    @ApiOperation("朋友圈评论")
    @PutMapping("comment")
    public R<Object> comment(@RequestBody CommentDTO commentDTO){
        friendsCircleService.comment(commentDTO);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

    @ApiOperation("发送朋友圈")
    @PutMapping("send")
    public R<Object> send(@RequestBody FriendsCircleDTO friendsCircleDTO){
        friendsCircleService.send(friendsCircleDTO);
        return new R<Object>().build(ResultEnum.RESULT_SUCCESS);
    }

}
