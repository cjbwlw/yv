package pers.cxl.app.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pers.cxl.app.service.ApplyFriendService;
import pers.cxl.common.result.R;
import pers.cxl.common.result.ResultEnum;
import pers.cxl.common.result.ResultPage;
import pers.cxl.model.app.entity.ApplyFriend;

@RestController("adminApplyFriendController")
@RequestMapping("admin/app/applyFriend")
public class ApplyFriendController {

    @Autowired
    private ApplyFriendService applyFriendService;

    @PostMapping("list")
    public R<ResultPage> list(@RequestBody ApplyFriend applyFriend){
        ResultPage resultPage = applyFriendService.pageList(applyFriend);
        return new R<ResultPage>().build(ResultEnum.RESULT_SUCCESS,resultPage);
    }

    @DeleteMapping("{id}")
    public R<Object> delByID(@PathVariable Long id){
        applyFriendService.removeById(id);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

}
