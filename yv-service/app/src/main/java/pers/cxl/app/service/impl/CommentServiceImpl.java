package pers.cxl.app.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.cxl.app.api.UserServiceApi;
import pers.cxl.app.mapper.CommentMapper;
import pers.cxl.app.service.CommentService;
import pers.cxl.app.service.FriendsCircleService;
import pers.cxl.common.constant.YVConstant;
import pers.cxl.common.result.R;
import pers.cxl.common.result.ResultPage;
import pers.cxl.common.util.PageHelper;
import pers.cxl.model.app.dto.CommentDTO;
import pers.cxl.model.app.dto.FriendsCircleDTO;
import pers.cxl.model.app.entity.Comment;
import pers.cxl.model.app.entity.FriendsCircle;
import pers.cxl.model.system.dto.UserDTO;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {

    @Autowired
    private PageHelper pageHelper;
    @Autowired
    private FriendsCircleService friendsCircleService;
    @Autowired
    private UserServiceApi userServiceApi;
    @Autowired
    private CommentMapper commentMapper;

    @Override
    public ResultPage pageList(CommentDTO commentDTO) {
        pageHelper.startPage(commentDTO);
        List<Comment> list = commentMapper.pageList(commentDTO);
        List<CommentDTO> dtoList = new ArrayList<>();
        list.forEach(c->{
            R<UserDTO> result = userServiceApi.getByID(c.getUid(), YVConstant.ADMIN);
            CommentDTO dto = new CommentDTO();
            BeanUtils.copyProperties(c,dto);
            dto.setUser(result.getData());
            FriendsCircle friendsCircle = friendsCircleService.getById(c.getFid());
            FriendsCircleDTO friendsCircleDTO = new FriendsCircleDTO();
            BeanUtils.copyProperties(friendsCircle,friendsCircleDTO);
            dto.setFriendsCircleDTO(friendsCircleDTO);
            dtoList.add(dto);
        });
        Long count = commentMapper.selectCount(null);
        return ResultPage.builder()
                .list(dtoList)
                .total(count)
                .build();
    }
}
