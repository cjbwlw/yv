package pers.cxl.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pers.cxl.app.api.UserServiceApi;
import pers.cxl.app.mapper.CommentMapper;
import pers.cxl.app.mapper.FriendsCircleMapper;
import pers.cxl.app.service.FriendsCircleLikeService;
import pers.cxl.app.service.FriendsCircleService;
import pers.cxl.common.constant.MessageConstant;
import pers.cxl.common.constant.YVConstant;
import pers.cxl.common.exception.GlobalException;
import pers.cxl.common.result.R;
import pers.cxl.common.result.ResultPage;
import pers.cxl.common.util.PageHelper;
import pers.cxl.model.app.dto.CommentDTO;
import pers.cxl.model.app.dto.FriendsCircleDTO;
import pers.cxl.model.app.entity.Comment;
import pers.cxl.model.app.entity.FriendsCircle;
import pers.cxl.model.app.entity.FriendsCircleLike;
import pers.cxl.model.system.dto.UserDTO;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Transactional
@Service
public class FriendsCircleServiceImpl extends ServiceImpl<FriendsCircleMapper, FriendsCircle> implements FriendsCircleService {

    @Autowired
    private UserServiceApi userServiceApi;
    @Autowired
    private FriendsCircleLikeService friendsCircleLikeService;
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private PageHelper pageHelper;

    @Override
    public List<FriendsCircleDTO> findList(Long id , Long myID) {
        // 查询当前用户的好友id列表
        R<List<Long>> data = userServiceApi.friendList(id);
        if (data.getCode() >= 5000){
            throw new GlobalException(MessageConstant.GET_FRIEND_LIST_ERR);
        }
        List<Long> friendIds = data.getData();
        friendIds.add(id);
        // 根据好友id查询朋友圈列表
        List<FriendsCircle> friendsCircles = new ArrayList<>();
        {
            LambdaQueryWrapper<FriendsCircle> wrapper = new LambdaQueryWrapper<FriendsCircle>()
                    .in(FriendsCircle::getUid, friendIds)
                    // 查询时间降序
                    .orderByDesc(FriendsCircle::getReleaseTime);
            // 用户好友所发的朋友圈列表
            friendsCircles = baseMapper.selectList(wrapper);
        }
        //  为朋友圈设置点赞列表
        List<FriendsCircleDTO> friendsCircleDTOS = new ArrayList<>();
        friendsCircles.forEach(i->{
            // 拷贝属性
            FriendsCircleDTO friendsCircleDTO = new FriendsCircleDTO();
            BeanUtils.copyProperties(i,friendsCircleDTO);
            // 查询当前朋友圈用户信息
            R<UserDTO> friendsCircleUser = userServiceApi.getByID(i.getUid(),myID);
            if (friendsCircleUser.getCode()>=5000){
                throw new GlobalException(MessageConstant.SERVER_ERR);
            }
            friendsCircleDTO.setUser(friendsCircleUser.getData());
            // 查询我对当前朋友圈是否点赞
            LambdaQueryWrapper<FriendsCircleLike> isLike = new LambdaQueryWrapper<FriendsCircleLike>()
                    .eq(FriendsCircleLike::getUid, id)
                    .eq(FriendsCircleLike::getFid, i.getId());
            FriendsCircleLike one = friendsCircleLikeService.getOne(isLike);
            friendsCircleDTO = one != null ? friendsCircleDTO.setLike(true) : friendsCircleDTO.setLike(false);
            // 查询点赞列表
            LambdaQueryWrapper<FriendsCircleLike> wrapper = new LambdaQueryWrapper<FriendsCircleLike>()
                    .eq(FriendsCircleLike::getFid, i.getId());
            // 用户和朋友圈点赞关系数据
            List<FriendsCircleLike> list = friendsCircleLikeService.list(wrapper);
            StringBuilder nameList = new StringBuilder();
            // 遍历用户数据设置点赞用户名称列表
            list.forEach(u->{
                // 根据用户id获取用户信息
                R<UserDTO> userData = userServiceApi.getByID(u.getUid(),myID);
                if (userData.getCode()>=5000){
                    throw new GlobalException(MessageConstant.SERVER_ERR);
                }
                String name = userData.getData().getNickname();
                nameList.append(name).append(",");
            });
            // 去除最后的逗号
            if (nameList.length()!=0){
                friendsCircleDTO.setLikes(new StringBuilder(nameList.substring(0, nameList.length() - 1)));
            }
            //  为朋友圈设置评论列表
            LambdaQueryWrapper<Comment> commentLambdaQueryWrapper = new LambdaQueryWrapper<Comment>()
                    .eq(Comment::getFid, i.getId()).orderByDesc(Comment::getReleaseTime);
            List<Comment> comments = commentMapper.selectList(commentLambdaQueryWrapper);
            // 查询并设置评论用户信息
            List<CommentDTO> commentDTOS = new ArrayList<>();
            comments.forEach(c->{
                R<UserDTO> userData = userServiceApi.getByID(c.getUid(),myID);
                if (userData.getCode()>=5000){
                    throw new GlobalException(MessageConstant.SERVER_ERR);
                }
                CommentDTO commentDTO = new CommentDTO();
                BeanUtils.copyProperties(c,commentDTO);
                commentDTO.setUser(userData.getData());
                commentDTOS.add(commentDTO);
            });
            friendsCircleDTO.setComments(commentDTOS);
            // 添加到dto集合中
            friendsCircleDTOS.add(friendsCircleDTO);
        });
        return friendsCircleDTOS;
    }

    @Override
    public void like(Integer flag,FriendsCircleLike friendsCircleLike) {
        if (flag == 1){
            friendsCircleLikeService.save(friendsCircleLike);
        }else {
            LambdaQueryWrapper<FriendsCircleLike> wrapper = new LambdaQueryWrapper<FriendsCircleLike>()
                    .eq(FriendsCircleLike::getUid, friendsCircleLike.getUid())
                    .eq(FriendsCircleLike::getFid, friendsCircleLike.getFid());
            friendsCircleLikeService.remove(wrapper);
        }
    }

    @Override
    public void comment(CommentDTO commentDTO) {
        Comment comment = new Comment();
        BeanUtils.copyProperties(commentDTO,comment);
        comment.setReleaseTime(LocalDateTime.now());
        commentMapper.insert(comment);
    }

    @Override
    public void send(FriendsCircleDTO friendsCircleDTO) {
        FriendsCircle friendsCircle = new FriendsCircle();
        BeanUtils.copyProperties(friendsCircleDTO,friendsCircle);
        friendsCircle.setReleaseTime(LocalDateTime.now());
        baseMapper.insert(friendsCircle);
    }

    @Override
    public ResultPage pageList(FriendsCircleDTO friendsCircleDTO) {
        // 查询发布朋友圈用户消息
        pageHelper.startPage(friendsCircleDTO);
        List<FriendsCircle> friendsCircles = baseMapper.pageList(friendsCircleDTO);
        List<FriendsCircleDTO> friendsCircleDTOS = new ArrayList<>();
        friendsCircles.forEach(f->{
            R<UserDTO> result = userServiceApi.getByID(f.getUid(), YVConstant.ADMIN);
            FriendsCircleDTO dto = new FriendsCircleDTO();
            BeanUtils.copyProperties(f,dto);
            dto.setUser(result.getData());
            friendsCircleDTOS.add(dto);
        });
        Long count = baseMapper.selectCount(null);
        return ResultPage.builder()
                .list(friendsCircleDTOS)
                .total(count)
                .build();
    }

    @Override
    public void delByID(Long id) {
        // 删除朋友圈点赞关系信息
        LambdaQueryWrapper<FriendsCircleLike> wrapper = new LambdaQueryWrapper<FriendsCircleLike>()
                .eq(FriendsCircleLike::getFid,id);
        friendsCircleLikeService.remove(wrapper);
        // 删除朋友圈评论关系信息
        LambdaQueryWrapper<Comment> commentLambdaQueryWrapper = new LambdaQueryWrapper<Comment>()
                .eq(Comment::getFid,id);
        commentMapper.delete(commentLambdaQueryWrapper);
        baseMapper.deleteById(id);
    }

}
