package pers.cxl.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.cxl.app.api.UserServiceApi;
import pers.cxl.app.mapper.ApplyFriendMapper;
import pers.cxl.app.service.ApplyFriendService;
import pers.cxl.common.constant.MessageConstant;
import pers.cxl.common.constant.YVConstant;
import pers.cxl.common.exception.GlobalException;
import pers.cxl.common.result.R;
import pers.cxl.common.result.ResultPage;
import pers.cxl.common.util.PageHelper;
import pers.cxl.model.app.entity.ApplyFriend;
import pers.cxl.model.system.dto.UserDTO;
import pers.cxl.model.system.entity.UF;

import java.util.List;

@Service
public class ApplyFriendServiceImpl extends ServiceImpl<ApplyFriendMapper, ApplyFriend> implements ApplyFriendService {

    @Autowired
    private UserServiceApi userServiceApi;
    @Autowired
    private ApplyFriendMapper applyFriendMapper;
    @Autowired
    private PageHelper pageHelper;

    @Override
    public void applyFriend(ApplyFriend applyFriend) {
        R<UserDTO> result = userServiceApi.getByID(applyFriend.getUid(), applyFriend.getUid());
        if (result.getCode() >= 5000) {
            throw new GlobalException(MessageConstant.RPC_ERR);
        }
        UserDTO userDTO = result.getData();
        applyFriend.setName(userDTO.getNickname())
                .setAvatar(userDTO.getAvatar());
        baseMapper.insert(applyFriend);
    }

    @Override
    public List<ApplyFriend> applyList(Long id) {
        LambdaQueryWrapper<ApplyFriend> wrapper = new LambdaQueryWrapper<ApplyFriend>()
                .eq(ApplyFriend::getFid, id);
        List<ApplyFriend> list = baseMapper.selectList(wrapper);
        return list;
    }

    @Override
    public void applyChange(ApplyFriend applyFriend) {
        if (applyFriend.getStatus()==1)
            userServiceApi.addFriend(new UF().setUid(applyFriend.getUid())
                    .setFid(applyFriend.getFid()));
        baseMapper.updateById(applyFriend);
    }

    @Override
    public ResultPage pageList(ApplyFriend applyFriend) {
        pageHelper.startPage(applyFriend);
        List<ApplyFriend> list = applyFriendMapper.pageList(applyFriend);
        list.forEach(a->{
            if (a.getFid()!=null){
                R<UserDTO> result = userServiceApi.getByID(a.getFid(), YVConstant.ADMIN);
                a.setFu(result.getData());
            }
        });
        Long count = baseMapper.selectCount(null);
        return ResultPage.builder()
                .list(list)
                .total(count)
                .build();
    }
}
