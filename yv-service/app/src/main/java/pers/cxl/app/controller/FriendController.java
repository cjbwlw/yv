package pers.cxl.app.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pers.cxl.app.api.UserServiceApi;
import pers.cxl.app.service.ApplyFriendService;
import pers.cxl.common.constant.MessageConstant;
import pers.cxl.common.exception.GlobalException;
import pers.cxl.common.result.R;
import pers.cxl.common.result.ResultEnum;
import pers.cxl.common.util.JWTUtil;
import pers.cxl.model.app.entity.ApplyFriend;
import pers.cxl.model.system.dto.UserDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(tags = "朋友")
@RestController
@RequestMapping("user/app/friend")
public class FriendController {

    @Autowired
    private UserServiceApi userServiceApi;
    @Autowired
    private ApplyFriendService applyFriendService;
    @Autowired
    private JWTUtil jwtUtil;

    @ApiOperation("处理好友申请")
    @PostMapping("apply/change")
    public R<List<ApplyFriend>> applyChange(@RequestBody ApplyFriend applyFriend){
        applyFriendService.applyChange(applyFriend);
        return new R<List<ApplyFriend>>().build(ResultEnum.RESULT_SUCCESS);
    }

    @ApiOperation("查询申请列表")
    @PostMapping("apply/list/{id}")
    public R<List<ApplyFriend>> applyList(@PathVariable Long id){
        List<ApplyFriend> data = applyFriendService.applyList(id);
        return new R<List<ApplyFriend>>().build(ResultEnum.RESULT_SUCCESS,data);
    }

    @ApiOperation("申请添加好友")
    @PostMapping("apply/friend")
    public R<Object> applyFriend(@RequestBody ApplyFriend applyFriend, HttpServletRequest request){
        Long id = jwtUtil.getId(request.getHeader("Token"));
        applyFriend.setUid(id);
        applyFriendService.applyFriend(applyFriend);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

    @ApiOperation("根据用户id查询朋友列表")
    @PostMapping("list/{id}")
    public R<List<UserDTO>> list(@PathVariable("id") Long id){
        R<List<UserDTO>> result = userServiceApi.friendListEntity(id);
        if (result.getCode()>=5000){
            throw new GlobalException(MessageConstant.RPC_ERR);
        }
        return new R<List<UserDTO>>().build(ResultEnum.RESULT_SUCCESS,result.getData());
    }

    @ApiOperation("根据用户账号用户")
    @PostMapping("search/{name}")
    public R<UserDTO> list(@PathVariable String name){
        R<UserDTO> result = userServiceApi.searchByAccount(name);
        if (result.getCode()>=5000){
            throw new GlobalException(MessageConstant.RPC_ERR);
        }
        return new R<UserDTO>().build(ResultEnum.RESULT_SUCCESS,result.getData());
    }

}
