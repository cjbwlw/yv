package pers.cxl.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import pers.cxl.common.result.ResultPage;
import pers.cxl.model.app.entity.ApplyFriend;

import java.util.List;

public interface ApplyFriendService extends IService<ApplyFriend> {

    void applyFriend(ApplyFriend applyFriend);

    List<ApplyFriend> applyList(Long id);

    void applyChange(ApplyFriend applyFriend);

    ResultPage pageList(ApplyFriend applyFriend);
}
