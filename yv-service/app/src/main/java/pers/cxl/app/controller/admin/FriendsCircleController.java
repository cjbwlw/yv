package pers.cxl.app.controller.admin;

import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pers.cxl.app.service.FriendsCircleService;
import pers.cxl.common.result.R;
import pers.cxl.common.result.ResultEnum;
import pers.cxl.common.result.ResultPage;
import pers.cxl.model.app.dto.FriendsCircleDTO;

@RestController("adminFriendCircle")
@RequestMapping("admin/app/friendCircle")
public class FriendsCircleController {

    @Autowired
    private FriendsCircleService friendsCircleService;

    @PostMapping("list")
    public R<ResultPage> list(@RequestBody FriendsCircleDTO friendsCircleDTO){
        ResultPage resultPage = friendsCircleService.pageList(friendsCircleDTO);
        return new R<ResultPage>().build(ResultEnum.RESULT_SUCCESS,resultPage);
    }

    @DeleteMapping("{id}")
    public R<Object> delByID(@PathVariable Long id){
        friendsCircleService.delByID(id);
        return new R<>().build(ResultEnum.RESULT_SUCCESS);
    }

}
