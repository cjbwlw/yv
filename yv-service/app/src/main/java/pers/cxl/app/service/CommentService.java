package pers.cxl.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import pers.cxl.common.result.ResultPage;
import pers.cxl.model.app.dto.CommentDTO;
import pers.cxl.model.app.entity.Comment;

public interface CommentService extends IService<Comment> {
    ResultPage pageList(CommentDTO commentDTO);

}
