package pers.cxl.app.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import pers.cxl.model.app.dto.FriendsCircleDTO;
import pers.cxl.model.app.entity.FriendsCircle;

import java.util.List;

@Mapper
public interface FriendsCircleMapper extends BaseMapper<FriendsCircle> {
    List<FriendsCircle> pageList(FriendsCircleDTO friendsCircleDTO);
}
