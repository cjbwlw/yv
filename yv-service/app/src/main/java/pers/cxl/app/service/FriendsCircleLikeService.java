package pers.cxl.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import pers.cxl.model.app.entity.FriendsCircleLike;

public interface FriendsCircleLikeService extends IService<FriendsCircleLike> {
}
