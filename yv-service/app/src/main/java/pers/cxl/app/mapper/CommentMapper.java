package pers.cxl.app.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import pers.cxl.model.app.dto.CommentDTO;
import pers.cxl.model.app.entity.Comment;

import java.util.List;

public interface CommentMapper extends BaseMapper<Comment> {

    List<Comment> pageList(CommentDTO commentDTO);

}
