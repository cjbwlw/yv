package pers.cxl.app.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import pers.cxl.app.mapper.FriendsCircleLikeMapper;
import pers.cxl.app.service.FriendsCircleLikeService;
import pers.cxl.model.app.entity.FriendsCircleLike;

@Service
public class FriendsCircleLikeServiceImpl extends ServiceImpl<FriendsCircleLikeMapper,FriendsCircleLike> implements FriendsCircleLikeService {
}
