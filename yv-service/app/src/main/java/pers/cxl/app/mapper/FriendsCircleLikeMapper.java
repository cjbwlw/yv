package pers.cxl.app.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import pers.cxl.model.app.entity.FriendsCircleLike;

@Mapper
public interface FriendsCircleLikeMapper extends BaseMapper<FriendsCircleLike> {
}
