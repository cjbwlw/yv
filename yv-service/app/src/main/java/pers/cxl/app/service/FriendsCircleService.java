package pers.cxl.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import pers.cxl.common.result.ResultPage;
import pers.cxl.model.app.dto.CommentDTO;
import pers.cxl.model.app.dto.FriendsCircleDTO;
import pers.cxl.model.app.entity.FriendsCircle;
import pers.cxl.model.app.entity.FriendsCircleLike;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface FriendsCircleService extends IService<FriendsCircle> {

    /**
     * 根据id查询朋友圈列表
     * @param id
     * @return
     */
    List<FriendsCircleDTO> findList(Long id, Long myID);

    /**
     * 用户朋友圈点赞
     * @param flag
     */
    void like(Integer flag, FriendsCircleLike friendsCircleLike);

    void comment(CommentDTO commentDTO);

    void send(FriendsCircleDTO friendsCircleDTO);

    ResultPage pageList(FriendsCircleDTO friendsCircleDTO);

    void delByID(Long id);
}
