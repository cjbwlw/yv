package pers.cxl.app.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import pers.cxl.model.app.entity.ApplyFriend;

import java.util.List;

@Mapper
public interface ApplyFriendMapper extends BaseMapper<ApplyFriend> {
    List<ApplyFriend> pageList(ApplyFriend applyFriend);
}
