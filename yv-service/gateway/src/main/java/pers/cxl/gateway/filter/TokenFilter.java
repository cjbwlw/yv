package pers.cxl.gateway.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.server.ServerWebExchange;
import pers.cxl.common.util.JWTUtil;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class TokenFilter implements GlobalFilter, Ordered {

    @Autowired
    private JWTUtil jwtUtil;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        String reqPath = request.getPath().toString();
        if (examinePath(reqPath)){
            return chain.filter(exchange);
        }
        HttpHeaders headers = request.getHeaders();
        List<String> token = headers.get("Token");
        if (!ObjectUtils.isEmpty(token)){
//            log.info("token是否有效===>{}",jwtUtil.parserToken(token.get(0)));
            if (jwtUtil.parserToken(token.get(0)))
                return chain.filter(exchange);
            else{
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                return response.setComplete();
            }
        }else {
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }
    }

    /**
     * 不需要验证token的接口
     */
    public boolean examinePath(String reqPath){
        ArrayList<String> list = new ArrayList<>();

        list.add("/admin/sso/login");
//        list.add("/admin/system/user/login");
        list.add("/user/app/user/registry");
        list.add("/admin/system/file/preview");
        list.add("/admin/system/file/upload");

        return list.contains(reqPath);
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
